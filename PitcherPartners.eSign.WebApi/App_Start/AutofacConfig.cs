﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Web.Http;
using System.Web.Mvc;
using Autofac;
using Autofac.Integration.Mvc;
using Autofac.Integration.WebApi;
using PitcherPartners.eSign.WebApi.Configuration;
using PitcherPartners.eSign.WebApi.Interfaces;
using PitcherPartners.eSign.WebApi.Models;
using PitcherPartners.eSign.WebApi.Services;
using PitcherPartners.eSign.WebClients.AdobeSign;

namespace PitcherPartners.eSign.WebApi
{
    /// <summary>
    /// Autofac configuration
    /// </summary>
    public static class AutofacConfig
    {
        /// <summary>
        /// Build base container with business-logic related things
        /// </summary>
        /// <returns></returns>
        public static void RegisterAutoFac()
        {
            var builder = new ContainerBuilder();

            AddMvcRegistrations(builder);
            AddRegisterations(builder);

            var container = builder.Build();
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
            System.Web.Http.GlobalConfiguration.Configuration.DependencyResolver = new AutofacWebApiDependencyResolver(container);
        }

        private static void AddMvcRegistrations(ContainerBuilder builder)
        {
            //mvc
            builder.RegisterControllers(Assembly.GetExecutingAssembly());
            builder.RegisterAssemblyModules(Assembly.GetExecutingAssembly());
            builder.RegisterModelBinders(Assembly.GetExecutingAssembly());
            builder.RegisterModelBinderProvider();

            //web api
            builder.RegisterAssemblyTypes(Assembly.GetExecutingAssembly()).PropertiesAutowired();
            builder.RegisterModule<AutofacWebTypesModule>();
        }

        private static void AddRegisterations(ContainerBuilder builder)
        {
            builder.RegisterType<ConfigurationSettings>().As<IConfigurationSettings>();

            builder.Register(b => new AdobeSignWebApiProxy { BaseUri = b.Resolve<IConfigurationSettings>().ApiAccessPoint }).As<IAdobeSignWebApiProxy>();

            builder.Register(b => new OAuthService(b.Resolve<IConfigurationSettings>().OAuthAccessPoint, 
                                                   b.Resolve<IConfigurationSettings>().WebAccessPoint)).As<IOAuthService>();

            builder.RegisterType<SignService>().As<ISignService>();
        }
    }
}
