﻿using PitcherPartners.eSign.WebClients.AdobeSign.Models;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Rest;
using RestSharp;

namespace PitcherPartners.eSign.WebApi.Interfaces
{
    public interface ISignService
    {
        void SetCredentials(string accessToken, string xApiUser);

        #region "Agreements"
        Task<AgreementCreationResponse> CreateAgreementAsync(AgreementCreationInfo agreementCreationInfo);
        Task<TransientDocumentResponse> CreateTransientDocumentAsync(System.IO.Stream fileStream, string fileName = default(string), string mimeType = default(string));
        Task<AgreementInfo> GetAgreementInfoAsync(string agreementId);
        Task<UserAgreements> GetAgreementsAsync(string query = default(string), string externalId = default(string), string externalGroup = default(string), string externalNamespace = default(string));
        Task<AgreementDocuments> GetAgreementDocumentsAsync(string agreementId, string versionId = null,string participantEmail = null, string supportingDocumentContentFormat = null);
        Task<Stream> GetAuditTrailAsync(string agreementId);
        Task<Stream> DownloadCombinedDocumentAsync(string agreementId, string versionId = null, string participantEmail = null, bool? attachSupportingDocuments = null, bool? auditReport = null);
        Task<DocumentImageUrls> GetCombinedDocumentImageUrlsAsync(string agreementId, string versionId = default(string), string participantEmail = default(string), string imageSizes = default(string), bool? includeSupportingDocumentsImageUrls = default(bool?), bool? showImageAvailabilityOnly = default(bool?));
        Task<DocumentUrl> GetCombinedDocumentUrlAsync(string agreementId, string versionId = default(string), string participantEmail = default(string), bool? attachSupportingDocuments = default(bool?), bool? auditReport = default(bool?));
        Task<DocumentImageUrl> GetDocumentImageUrlsAsync(string agreementId, string documentId, string versionId = default(string), string participantEmail = null, string imageSizes = default(string), bool? showImageAvailabilityOnly = default(bool?), int? startPage = null, int? endPage = null);

        AgreementStatusUpdateResponse CancelAgreement(string agreementId, string comment = default(string), bool? notifySigner = default(bool?));
        Task DeleteAgreement(string agreementId);
    
        #endregion
 
        #region "Search"
        Task<AgreementAssetEventPostResponse> CreateAssetEventAsync(AgreementAssetEventRequest agreementAssetEventRequest);
        Task<AgreementAssetEventGetResponse> GetAssetEventAsync(string searchId, string pageCursor, int? pageSize = null);
        #endregion

        #region "Reminders"
        Task<ReminderCreationResult> CreateReminderAsync(ReminderCreationInfo reminderCreationInfo);
        #endregion

        #region "Users"
        Task<UsersInfo> GetUsersAsync(string email = default(string));
        Task<UserDetailsInfo> GetUserDetailAsync(string userId);


        #endregion

    }
}
