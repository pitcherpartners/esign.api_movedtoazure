﻿using PitcherPartners.eSign.WebApi.Models.OAuth;
using PitcherPartners.eSign.WebClients.Casu.Models;
using System;
using System.Threading.Tasks;

namespace PitcherPartners.eSign.WebApi.Interfaces
{
    public interface IOAuthService
    {
        bool RequiresAuthentication(string userName);
        Uri GetAuthorizationUrl(AuthorizationRequest authorizationRequest);
        AccessTokenResponse GetAccessToken(AccessTokenRequest accessTokenRequest);
        AccessTokenRefreshResponse RefreshAccessToken(AccessTokenRefreshRequest accessTokenRefreshRequest);
        Task<UserExternalAuthenticationEntity> ProcessAuthenticationCodeAsync(string code, string username);
    }
}
