﻿using PitcherPartners.eSign.WebApi.Configuration;
using PitcherPartners.eSign.WebApi.Interfaces;
using PitcherPartners.eSign.WebApi.Models.OAuth;
using PitcherPartners.eSign.WebApi.Utilities;
using PitcherPartners.eSign.WebClients.Casu;
using System;
using System.Linq;
using System.Runtime.Caching;
using System.Threading.Tasks;
using System.Web.Mvc;
using PitcherPartners.eSign.WebClients.Casu.Models;

namespace PitcherPartners.eSign.WebApi.Models
{

    /// <summary>
    /// Represents a User in AdobeSign and contains all the configuration to access the AdobeSign API. 
    /// </summary>
    public class AdobeSignUser
    {

        private readonly string _userName;

        private ISignService _signService;
        public ISignService SignService => _signService ?? (_signService = DependencyResolver.Current.GetService<ISignService>());

        private IOAuthService _oAuthService;
        public IOAuthService OAuthService => _oAuthService ?? (_oAuthService = DependencyResolver.Current.GetService<IOAuthService>());


        private IConfigurationSettings _configurationSettings;
        public IConfigurationSettings ConfigurationSettings => _configurationSettings ?? (_configurationSettings = DependencyResolver.Current.GetService<IConfigurationSettings>());


        #region Cache
        private readonly CacheManager _cacheManager = new CacheManager();
        private readonly MemoryCache _cache = MemoryCache.Default;
        /// <summary>
        /// The cache key format
        /// </summary>
        public const string CacheKeyFormat = "AdobeSignUser_{0}";
        #endregion


        /// <summary>
        /// Constructor for when using oauth2
        /// </summary>
        /// <param name="userName">Name of the user.</param>
        public AdobeSignUser(string userName)
        {
            //User Configuration
            _userName = userName;

            //Default Cache Policy
            _cacheManager.DefaultCachePolicy = new CacheItemPolicy
            {
                Priority = CacheItemPriority.Default,
                SlidingExpiration = TimeSpan.FromHours(2),
            };
        }

        /// <summary>
        /// Return the current user name.
        /// </summary>
        public string UserName
        {
            get { return _userName; }
        }


        /// <summary>
        /// Authenticate the user in the AdobeSign API using OAuth
        /// </summary>
        public void TokenAuthentication()
        {
            // bypass OAuth authentication use Integration Key instead for calling Swagger directly.
            if (ConfigurationSettings.UseIntegrationKey) // debug only
            {
                SignService.SetCredentials(ConfigurationSettings.IntegrationKey, null);
                return;
            }

            var cached = (UserExternalAuthenticationEntity)_cache.Get(string.Format(CacheKeyFormat, _userName).ToLowerInvariant());

            // The cached token needs to have a valid Access Token and not expired.
            if (cached?.AccessToken != null && cached.ExpirationDateTime > DateTime.Now)
            {
                SignService.SetCredentials(cached.AccessToken, _userName);
                return;
            }
            else
            {
                var casuEndpoint = new Uri(ConfigurationSettings.CasuEndpoint);
                using (var casuApi = new PitcherPartnersCASUWeb(baseUri: casuEndpoint))
                {
                    var casuApps = casuApi.Application.Get();
                    var esignApp = casuApps.FirstOrDefault(a => a.IdsApplicationId == "ESIGN");
                    if (esignApp?.ApplicationId == null)
                    {
                        throw new Exception("Can't find ESIGN application!");
                    }

                    var userAuthDetails = casuApi.UserExternalAuthentication.GetByKey(_userName, esignApp.ApplicationId.Value);
                    if (userAuthDetails?.ExpirationDateTime == null)
                    {
                        throw new Exception($"ESIGN authentication record can not be found with Username: {_userName} AppId: {esignApp.ApplicationId.Value}");
                    }

                    // Expired tokens
                    if (userAuthDetails.ExpirationDateTime <= DateTime.Now)
                    {
                        var accessTokenRefreshRequest = new AccessTokenRefreshRequest()
                        {
                            ClientId = ConfigurationSettings.ClientId,
                            ClientSecret = ConfigurationSettings.ClientSecret,
                            GrantType = "authorization_code",
                            RefreshToken = userAuthDetails.RefreshToken,
                            RedirectUri = ConfigurationSettings.OAuthRedirectUrl,
                            Code = userAuthDetails.Code
                        };

                        var accessTokenRefreshResponse = OAuthService.RefreshAccessToken(accessTokenRefreshRequest);
                        if (accessTokenRefreshResponse?.AccessToken == null)
                        {
                            throw new Exception($"Failed to refresh access token with Username: {_userName} Refresh Token: {userAuthDetails.RefreshToken}");
                        }



                        var userExternalAuthenticationEntity = new UserExternalAuthenticationEntity()
                        {
                            UserName = _userName,
                            AccessToken = accessTokenRefreshResponse.AccessToken,
                            ApplicationId = esignApp.ApplicationId,
                            ExpirationDateTime = DateTime.Now.AddSeconds(accessTokenRefreshResponse.ExpiresIn),
                            LastUpdateDateTime = DateTime.Now,
                            //check if the refresh token refreshs
                            RefreshToken = userAuthDetails.RefreshToken,
                            Code = userAuthDetails.Code
                        };

                        var result = casuApi.UserExternalAuthentication.Post(userExternalAuthenticationEntity);

                        _cacheManager.CleanCacheEntry(MemoryCache.Default, string.Format(CacheKeyFormat, result.UserName).ToLowerInvariant());

                        if (result.ExpirationDateTime != null)
                            _cacheManager.SetToCache(_cache,
                                string.Format(CacheKeyFormat, result.UserName).ToLowerInvariant(), result,
                                new CacheItemPolicy {AbsoluteExpiration = result.ExpirationDateTime.Value});

                        // Set credentials.
                        SignService.SetCredentials(result.AccessToken, result.UserName);
                    }
                    else
                    {
                        _cacheManager.SetToCache(_cache, string.Format(CacheKeyFormat, _userName).ToLowerInvariant(), userAuthDetails,
                            new CacheItemPolicy { AbsoluteExpiration = userAuthDetails.ExpirationDateTime.Value });

                        // Set credentials.
                        SignService.SetCredentials(userAuthDetails.AccessToken, userAuthDetails.UserName);
                    }
                }
            }
        }
    }
}
