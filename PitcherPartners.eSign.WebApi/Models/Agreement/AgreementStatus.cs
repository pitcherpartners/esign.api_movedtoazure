﻿namespace PitcherPartners.eSign.WebApi.Models.Agreement
{
    public class AgreementStatus
    {
        public string Status { get; set; }
        public int Count { get; set; }
    }
}