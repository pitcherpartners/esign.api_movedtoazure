﻿namespace PitcherPartners.eSign.WebApi.Models.OAuth
{
    public class AccessTokenResponse
    {
        public string AccessToken { get; set; }

        public string RefreshToken { get; set; }

        public int ExpiresIn { get; set; } = -1;

        public string TokenType { get; set; }
    }

}
