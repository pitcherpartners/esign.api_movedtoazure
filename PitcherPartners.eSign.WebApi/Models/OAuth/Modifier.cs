﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PitcherPartners.eSign.WebApi.Models.OAuth
{
    public class Modifier
    {
        public const string Self = "self";
        public const string Group = "group";
        public const string Account = "account";
    }
}