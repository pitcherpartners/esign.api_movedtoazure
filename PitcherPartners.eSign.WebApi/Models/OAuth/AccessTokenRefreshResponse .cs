﻿namespace PitcherPartners.eSign.WebApi.Models.OAuth
{
    public class AccessTokenRefreshResponse
    {
        public string AccessToken { get; set; }

        public int ExpiresIn { get; set; } = -1;

        public string TokenType { get; set; }
    }

}
