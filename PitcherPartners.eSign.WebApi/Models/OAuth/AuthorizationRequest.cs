﻿using System.Collections.Generic;

namespace PitcherPartners.eSign.WebApi.Models.OAuth
{
    public class AuthorizationRequest
    {
        public string ClientId { get; set; }
        public string RedirectUri { get; set; }
        public List<Scope> Scopes { get; set; }
        public string State { get; set; }
        public string ResponseType { get; set; }

    }




   
}
