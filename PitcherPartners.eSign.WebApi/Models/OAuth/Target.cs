﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PitcherPartners.eSign.WebApi.Models.OAuth
{
    public class Target
    {
        public const string AgreementRead = "agreement_read";
        public const string AgreementSend = "agreement_send";

        public const string AgreementWrite = "agreement_write";

        //public const string AgreementSign = "agreement_sign";
        public const string AgreementRetention = "agreement_retention";

        public const string LibraryRead = "library_read";
        public const string LibraryWrite = "library_write";
        public const string LibraryRetention = "library_retention";
        public const string UserLogin = "user_login";
        public const string UserRead = "user_read";
        public const string UserWrite = "user_write";
        public const string WidgetRead = "widget_read";
        public const string WidgetWrite = "widget_write";
        public const string WorkflowRead = "workflow_read";
        public const string WorkflowWrite = "workflow_write";
        


        /// <summary>
        /// Aggregates all possible Targets
        /// </summary>
        /// <returns></returns>
        public static List<string> All() { 
 
            List<string> lstofConstants = new List<string>();
            foreach (var constant in typeof(Target).GetFields())
            {
                if (constant.IsLiteral && !constant.IsInitOnly)
                {
                    lstofConstants.Add((string)constant.GetValue(null));
                }
            };

            return lstofConstants;
        }
    }
}