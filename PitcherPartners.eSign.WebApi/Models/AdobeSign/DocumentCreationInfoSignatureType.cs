﻿namespace PitcherPartners.eSign.WebApi.Models.AdobeSign
{
    public class DocumentCreationInfoSignatureType
    {
        public const string Esign = "ESIGN";
        public const string Written = "WRITTEN";
    }
}