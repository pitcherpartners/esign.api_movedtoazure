﻿namespace PitcherPartners.eSign.WebApi.Models.AdobeSign
{
    public class Recipient
    {
        public string Email { get; set; }
        public int SigningOrder { get; set; }
    }

}
