﻿namespace PitcherPartners.eSign.WebApi.Models.AdobeSign
{
    public class RecipientSetRole
    {
        public const string Signer = "SIGNER";
        public const string Approver = "APPROVER";
        public const string Acceptor = "ACCEPTOR";
        public const string FormFiller = "FORM_FILLER";
        public const string CertifiedRecipient = "CERTIFIED_RECIPIENT";
        public const string DelegateToSigner = "DELEGATE_TO_SIGNER";
        public const string DelegateToApprover = "DELEGATE_TO_APPROVER";
        public const string DelegateToAcceptor = "DELEGATE_TO_ACCEPTOR";
        public const string DelegateToFormFiller = "DELEGATE_TO_FORM_FILLER";
        public const string DelegateToCertifiedRecipient = "DELEGATE_TO_CERTIFIED_RECIPIENT";
    }
}