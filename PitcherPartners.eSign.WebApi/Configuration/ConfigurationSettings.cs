﻿using System;
using System.Configuration;

namespace PitcherPartners.eSign.WebApi.Configuration
{
    public interface IConfigurationSettings
    {
        Uri ApiAccessPoint { get; set; }
        Uri WebAccessPoint { get; set; }
        Uri OAuthAccessPoint { get; set; }


        string CasuEndpoint { get; set; }
        string StaffAuthKey { get; set; }
        string IntegrationKey { get; set; }


        string ClientId { get; set; }
        string ClientSecret { get; set; }

        string OAuthRedirectUrl { get; set; }
        string OAuthCallbackUrl { get; set; }

        string EventCallback { get; set; }
        string PostSignRedirectUrl { get; set; }

        bool UseIntegrationKey { get; set; }
    }

    public class ConfigurationSettings : IConfigurationSettings
    {
        public ConfigurationSettings()
        {

            ApiAccessPoint = new Uri(ConfigurationManager.AppSettings[ConfigKeys.ApiAccessPoint]);
            WebAccessPoint = new Uri(ConfigurationManager.AppSettings[ConfigKeys.WebAccessPoint]);
            OAuthAccessPoint = new Uri(ConfigurationManager.AppSettings[ConfigKeys.OAuthAccessPoint]);


            ClientId = ConfigurationManager.AppSettings[ConfigKeys.ClientId];
            ClientSecret = ConfigurationManager.AppSettings[ConfigKeys.ClientSecret];

            CasuEndpoint = ConfigurationManager.AppSettings[ConfigKeys.CasuEndpoint];
            StaffAuthKey = ConfigurationManager.AppSettings[ConfigKeys.StaffAuthKey];
            IntegrationKey = ConfigurationManager.AppSettings[ConfigKeys.IntegrationKey];
            UseIntegrationKey = Convert.ToBoolean(ConfigurationManager.AppSettings[ConfigKeys.UseIntegrationKey]);

            OAuthRedirectUrl = ConfigurationManager.AppSettings[ConfigKeys.OAuthRedirectUrl];
            OAuthCallbackUrl = ConfigurationManager.AppSettings[ConfigKeys.OAuthCallbackUrl];

            EventCallback = ConfigurationManager.AppSettings[ConfigKeys.EventCallback];
            PostSignRedirectUrl = ConfigurationManager.AppSettings[ConfigKeys.PostSignRedirectUrl];
        }

        public Uri ApiAccessPoint { get; set; }
        public Uri WebAccessPoint { get; set; }
        public Uri OAuthAccessPoint { get; set; }


        public string CasuEndpoint { get; set; }
        public string StaffAuthKey { get; set; }
        public string IntegrationKey { get; set; }

        public bool UseIntegrationKey { get; set; }

        public string ClientId { get; set; }
        public string ClientSecret { get; set; }

        public string OAuthRedirectUrl { get; set; }
        public string OAuthCallbackUrl { get; set; }

        public string EventCallback { get; set; }
        public string PostSignRedirectUrl { get; set; }



        /// <summary>
        /// Magic strings class to keep it in one place
        /// </summary>
        private static class ConfigKeys
        {
            public const string ApiAccessPoint = "AdobeSign.ApiAccessPoint";
            public const string WebAccessPoint = "AdobeSign.WebAccessPoint";
            public const string OAuthAccessPoint = "AdobeSign.OAuthAccessPoint";

            public const string CasuEndpoint = "Api.CASU.Endpoint";
            public const string StaffAuthKey = "Api.ESIGN.StaffAuthKey";
            public const string IntegrationKey = "AdobeSign.IntegrationKey";
            public const string UseIntegrationKey = "UseIntegrationKey";

            public const string ClientId = "AdobeSign.ClientId";
            public const string ClientSecret = "AdobeSign.ClientSecret";

            public const string OAuthRedirectUrl = "AdobeSign.OAuthRedirectUrl";
            public const string OAuthCallbackUrl = "AdobeSign.OAuthCallbackUrl";

            public const string EventCallback = "AdobeSign.EventCallback";
            public const string PostSignRedirectUrl = "AdobeSign.PostSignRedirectUrl";
        }
    }
}
