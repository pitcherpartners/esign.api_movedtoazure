﻿using PitcherPartners.eSign.WebApi.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Filters;
using System.Web.Mvc;
using PitcherPartners.eSign.WebApi.Configuration;

namespace PitcherPartners.eSign.WebApi.Filters
{
    public class AdobeSignAuthenticationFilter : Attribute, IAuthenticationFilter
    {
        public Task AuthenticateAsync(HttpAuthenticationContext context, CancellationToken cancellationToken)
        {
            HttpRequestMessage request = context.Request;

            AuthenticationHeaderValue authorization = request.Headers.Authorization;

            var configurationSettings = DependencyResolver.Current.GetService<IConfigurationSettings>();
            
            // bypass OAuth authentication use Integration Key instead for calling Swagger directly.
            if (configurationSettings.UseIntegrationKey) // debug only
            {
                var adobeSignUser = new AdobeSignUser("calvin.zhang@pitcher.com.au");

                try
                {
                    adobeSignUser.TokenAuthentication();

                    request.Properties.Add(new KeyValuePair<string, object>("AdobeSignUser", adobeSignUser));
                }
                catch (Exception ex)
                {
                    context.ErrorResult = new AuthenticationFailureResult(ex.Message, request);
                }
            }
            else
            {
                if (authorization == null || authorization.Scheme != "Basic" || string.IsNullOrWhiteSpace(authorization.Parameter))
                {
                    context.ErrorResult = new AuthenticationFailureResult("Missing credentials", request);
                }
                else
                {
                    var userNameAndPassword = ExtractUserNameAndPassword(authorization.Parameter);

                    var userName = userNameAndPassword.Item1;
                    var password = userNameAndPassword.Item2;

                    if (userName.EndsWith(@"@pitcher.com.au")) //need to figure out a better way to determine this
                    {
                        // extra layer of security so you can't just authenticate with any staff email address without passing the key

                        var staffAuthKey = configurationSettings.StaffAuthKey;

                        if (password != staffAuthKey)
                        {
                            context.ErrorResult =
                                new AuthenticationFailureResult(
                                    $"Missing or incorrect staff authentication key: {staffAuthKey}", request);
                        }

                        var adobeSignUser = new AdobeSignUser(userName);

                        try
                        {
                            adobeSignUser.TokenAuthentication();

                            request.Properties.Add(new KeyValuePair<string, object>("AdobeSignUser", adobeSignUser));
                        }
                        catch (Exception ex)
                        {
                            context.ErrorResult = new AuthenticationFailureResult(ex.Message, request);
                        }
                    }
                }
            }

            return Task.FromResult(0); 
        }

        private Tuple<string, string> ExtractUserNameAndPassword(string parameter)
        {
            var authHeader = Encoding.Default.GetString(Convert.FromBase64String(parameter));

            var tokens = authHeader.Split(new[] { ':' }, 2);
            return new Tuple<string, string>(tokens.First(), tokens.Last());
        }



        public Task ChallengeAsync(HttpAuthenticationChallengeContext context, CancellationToken cancellationToken)
        {
            return Task.FromResult(0);
        }

        public bool AllowMultiple => false;
    }
    public class AuthenticationFailureResult : IHttpActionResult
    {
        public AuthenticationFailureResult(string reasonPhrase, HttpRequestMessage request)
        {
            ReasonPhrase = reasonPhrase;
            Request = request;
        }

        public string ReasonPhrase { get; private set; }


        public HttpRequestMessage Request { get; private set; }


        public Task<HttpResponseMessage> ExecuteAsync(CancellationToken cancellationToken)
        {
            return Task.FromResult(Execute());
        }

        private HttpResponseMessage Execute()
        {
            HttpResponseMessage response =
                new HttpResponseMessage(HttpStatusCode.Unauthorized)
                {
                    RequestMessage = Request,
                    ReasonPhrase = ReasonPhrase
                };
            return response;
        }
    }

}