﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http.ExceptionHandling;
using System.Web.Http.Filters;
using Microsoft.Rest;
using Newtonsoft.Json;
using PitcherPartners.eSign.WebApi.Utilities;

namespace PitcherPartners.eSign.WebApi.Filters
{
    /// <summary>
    /// Global exception handler for Web API.
    /// It is being put on the BaseApiController which is inherited across all controllers, except OAuthController.
    /// </summary>
    /// <seealso cref="System.Web.Http.Filters.ExceptionFilterAttribute" />
    public class WebApiExceptionFilter : ExceptionFilterAttribute
    {
        /// <summary>
        /// Raises the exception event.
        /// </summary>
        /// <param name="actionExecutedContext">The context for the action.</param>
        public override void OnException(HttpActionExecutedContext actionExecutedContext)
        {
            if (actionExecutedContext.Exception is HttpOperationException)
            {
                var ex = actionExecutedContext.Exception as HttpOperationException;
                actionExecutedContext.Response = new HttpResponseMessage()
                {
                    StatusCode = ex.Response.StatusCode,
                    Content = new StringContent(ex.Message)
                };
            }
            else
            {
                // 500 - Internal Server Error
                actionExecutedContext.Response = new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    StatusCode = HttpStatusCode.InternalServerError,
                    Content = new StringContent(!string.IsNullOrEmpty(actionExecutedContext.Exception.Message) ? actionExecutedContext.Exception.Message : "We are sorry, something went wrong")
                };
            }
        }
    }
}
