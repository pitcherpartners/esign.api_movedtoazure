﻿using System;
using PitcherPartners.eSign.WebApi.Filters;
using PitcherPartners.eSign.WebApi.Models.Agreement;
using PitcherPartners.eSign.WebClients.AdobeSign.Models;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web.Http;
//using Swashbuckle.Swagger.Annotations;

namespace PitcherPartners.eSign.WebApi.Controllers
{
    [AdobeSignAuthenticationFilter]
    [RoutePrefix("api/users")]
    public class UserController : BaseApiController
    {
        /// <summary>
        /// Gets all the users in an account.
        /// </summary>
        /// <param name="email">User email address</param>
        /// <returns>
        /// UsersInfo
        /// </returns>
        /// <exception cref="HttpResponseException"></exception>
        [HttpGet]
        [Route("")]
        public async Task<UsersInfo> GetUsers(string email = default(string))
        {
            var usersInfo = await CurrentUser.SignService.GetUsersAsync(email);
            
            return usersInfo;
        }

        [HttpGet]
        [Route("{userId}")]
        public async Task<UserDetailsInfo> GetUserDetails(string userId)
        {
            var userDetailsInfo = await CurrentUser.SignService.GetUserDetailAsync(userId);

            return userDetailsInfo;
        }

    }
}