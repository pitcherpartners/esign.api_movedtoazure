﻿using PitcherPartners.eSign.WebApi.Filters;
using PitcherPartners.eSign.WebClients.AdobeSign.Models;
using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Microsoft.Rest;

namespace PitcherPartners.eSign.WebApi.Controllers
{
    [AdobeSignAuthenticationFilter]
    [RoutePrefix("api/reminders")]
    public class ReminderController : BaseApiController
    {
        /// <summary>
        /// Sends a reminder for an agreement.
        /// </summary>
        /// <returns>ReminderCreationResult</returns>
        /// <exception cref="HttpResponseException"></exception>
        /// <response code="201">Created.</response>
        [HttpPost]
        [Route("create")]
        public async Task<ReminderCreationResult> CreateReminder(ReminderCreationInfo reminderCreationInfo)
        {
            var reminderCreationResult = await CurrentUser.SignService.CreateReminderAsync(reminderCreationInfo);

            return reminderCreationResult;
        }
    }
}
