﻿using System;
using PitcherPartners.eSign.WebApi.Filters;
using PitcherPartners.eSign.WebApi.Models.Agreement;
using PitcherPartners.eSign.WebClients.AdobeSign.Models;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web.Http;
//using Swashbuckle.Swagger.Annotations;

namespace PitcherPartners.eSign.WebApi.Controllers
{
    [AdobeSignAuthenticationFilter]
    [RoutePrefix("api/agreements")]
    public class AgreementController : BaseApiController
    {
        /// <summary>
        /// Create an agreement.
        /// </summary>
        /// <param name="agreementCreationInfo">The agreement creation information.</param>
        /// <returns>
        /// AgreementCreationResponse
        /// </returns>
        /// <exception cref="HttpResponseException"></exception>
        /// <response code="200">Created.</response>
        [HttpPost]
        [Route("create")]
        public async Task<AgreementCreationResponse> CreateAgreement([FromBody]AgreementCreationInfo agreementCreationInfo)
        {
            // Set defaults
            if (agreementCreationInfo.Options == null)
            {
                agreementCreationInfo.Options =
                    new InteractiveOptions {AuthoringRequested = true, AutoLoginUser = true, NoChrome = true};
            }


            if (agreementCreationInfo.DocumentCreationInfo.CallbackInfo == null)
            {
                agreementCreationInfo.DocumentCreationInfo.CallbackInfo = ConfigurationSettings.EventCallback;
            }

            //  Gets and Sets URL and associated properties for the success page the user will be taken to after completing the signing process
            if (agreementCreationInfo.DocumentCreationInfo.PostSignOptions == null)
            {
                agreementCreationInfo.DocumentCreationInfo.PostSignOptions =
                    new PostSignOptions {RedirectUrl = ConfigurationSettings.PostSignRedirectUrl};
            }


            var agreementCreationResponse = await CurrentUser.SignService.CreateAgreementAsync(agreementCreationInfo);
            
            return agreementCreationResponse;
        }


        /// <summary>
        /// Creates the transient document.
        /// </summary>
        /// <param name="file">File bytes</param>
        /// <param name="fileName">Name of the file.</param>
        /// <param name="mimeType">Type of the MIME.</param>
        /// <returns>
        /// TransientDocumentResponse
        /// </returns>
        /// <response code="200">Created.</response>
        [HttpPost]
        [Route("createTransientDocument")]
        public async Task<TransientDocumentResponse> CreateTransientDocument(byte[] file, string fileName = default(string), string mimeType = default(string))
        {
            Stream fileStream = new MemoryStream(file);

            TransientDocumentResponse transientDocumentResponse = await CurrentUser.SignService.CreateTransientDocumentAsync(fileStream, fileName, mimeType);

            return transientDocumentResponse;
        }

        /// <summary>
        /// Retrieves the latest status of an agreement.
        /// </summary>
        /// <param name="agreementId">The agreement identifier.</param>
        /// <returns>
        /// AgreementInfo
        /// </returns>
        /// <exception cref="HttpResponseException"></exception>
        [HttpGet]
        [Route("{agreementId}")]
        public async Task<AgreementInfo> GetAgreementInfo(string agreementId)
        {
            var agreementInfo = await CurrentUser.SignService.GetAgreementInfoAsync(agreementId);

            return agreementInfo;
        }

        /// <summary>
        ///     Retrieves agreements for the user.
        /// </summary>
        /// <param name="query">
        ///     The query string used for the search. Multiple search terms can be provided, separated by spaces.
        ///     Some of the search terms include document name, participant name or company, and form data.
        /// </param>
        /// <param name="externalId">
        ///     Case-sensitive ExternalID for which you would like to retrieve agreement information.
        ///     ExternalId is passed in the call to the agreement creation API.
        /// </param>
        /// <param name="externalGroup">
        ///     ExternalGroup for which you would like to retrieve agreement information. ExternalGroup is
        ///     passed in the call to the agreement creation API. You must pass ExternalId if passing ExternalGroup parameter.
        /// </param>
        /// <param name="externalNamespace">
        ///     ExternalNameSpace for which you would like to retrieve agreement information.
        ///     ExternalNameSpace is passed in the call to the agreement creation API. You must pass ExternalId if passing
        ///     ExternalNameSpace parameter.
        /// </param>
        /// <returns>
        ///     AgreementInfo
        /// </returns>
        /// <exception cref="HttpResponseException"></exception>
        /// <exception cref="HttpResponseMessage"></exception>
        /// <exception cref="StringContent"></exception>
        [HttpGet]
        [Route("")]
        public async Task<UserAgreements> GetAgreements(string query = null, string externalId = null, string externalGroup = null, string externalNamespace = null)
        {
            var userAgreements = await CurrentUser.SignService.GetAgreementsAsync(query, externalId, externalGroup, externalNamespace);

            return userAgreements;
        }

        /// <summary>
        ///     Retrieves the count for each agreement status
        /// </summary>
        /// <returns>AgreementInfo</returns>
        /// <exception cref="HttpResponseException"></exception>
        [HttpGet]
        [Route("statusCount")]
        public async Task<List<AgreementStatus>> GetStatusCount()
        {
            var userAgreements = await CurrentUser.SignService.GetAgreementsAsync();

            var groupedStatuses = (from u in userAgreements.UserAgreementList
                                   group u by u.Status
                into grp
                                   select new AgreementStatus
                                   {
                                       Status = grp.Key,
                                       Count = grp.Count()
                                   }).ToList();

            return groupedStatuses;
        }

        /// <summary>
        ///     Retrieves the IDs of all the main and supporting documents of an agreement identified by agreementid.
        /// </summary>
        /// <param name="agreementId">The agreement identifier.</param>
        /// <param name="versionId">The version identifier.</param>
        /// <param name="participantEmail">The participant email.</param>
        /// <param name="supportingDocumentContentFormat">The supporting document content format.</param>
        /// <returns></returns>
        /// <exception cref="HttpResponseException"></exception>
        /// <exception cref="HttpResponseMessage"></exception>
        /// <exception cref="StringContent"></exception>
        [HttpGet]
        [Route("{agreementId}/documents")]
        public async Task<AgreementDocuments> GetDocuments(string agreementId, string versionId = null, string participantEmail = null, string supportingDocumentContentFormat = null)
        {
            var agreementDocuments = await CurrentUser.SignService.GetAgreementDocumentsAsync(agreementId, versionId, participantEmail, supportingDocumentContentFormat);

            return agreementDocuments;
        }

        /// <summary>
        ///     Retrieves image urls of all visible pages of all the documents associated with an agreement.
        /// </summary>
        /// <param name="agreementId">The agreement identifier.</param>
        /// <param name="versionId">The version identifier.</param>
        /// <param name="participantEmail">The participant email.</param>
        /// <param name="imageSizes">
        ///     A comma separated list of image sizes i.e. {FIXED_WIDTH_50px, FIXED_WIDTH_250px,
        ///     FIXED_WIDTH_675px, ZOOM_50_PERCENT, ZOOM_75_PERCENT, ZOOM_100_PERCENT, ZOOM_125_PERCENT, ZOOM_150_PERCENT,
        ///     ZOOM_200_PERCENT}. Default sizes returned are {FIXED_WIDTH_50px, FIXED_WIDTH_250px, FIXED_WIDTH_675px,
        ///     ZOOM_100_PERCENT}.
        /// </param>
        /// <param name="includeSupportingDocumentsImageUrls">The include supporting documents image urls.</param>
        /// <param name="showImageAvailabilityOnly">The show image availability only.</param>
        /// <returns>DocumentImageUrls</returns>
        /// <exception cref="HttpResponseException"></exception>
        /// <exception cref="HttpResponseMessage"></exception>
        /// <exception cref="StringContent"></exception>
        [HttpGet]
        [Route("{agreementId}/documents/imageUrls")]
        public async Task<DocumentImageUrls> GetDocumentsImageUrls(string agreementId, string versionId = default(string), string participantEmail = default(string), string imageSizes = default(string), bool? includeSupportingDocumentsImageUrls = default(bool?), bool? showImageAvailabilityOnly = default(bool?))
        {
            var documentImageUrls = await CurrentUser.SignService.GetCombinedDocumentImageUrlsAsync(agreementId, versionId, participantEmail, imageSizes, includeSupportingDocumentsImageUrls, showImageAvailabilityOnly);

            return documentImageUrls;
        }

        /// <summary>
        ///     Gets the combined document PDF download link.
        /// </summary>
        /// <param name="agreementId">The agreement identifier.</param>
        /// <param name="versionId">The version identifier.</param>
        /// <param name="participantEmail">The participant email.</param>
        /// <param name="attachSupportingDocuments">The attach supporting documents.</param>
        /// <param name="auditReport">The audit report.</param>
        /// <returns></returns>
        /// <exception cref="HttpResponseException"></exception>
        /// <exception cref="HttpResponseMessage"></exception>
        /// <exception cref="StringContent"></exception>
        [HttpGet]
        [Route("{agreementId}/combinedDocument/downloadUrl")]
        public async Task<DocumentUrl> GetCombinedDocumentUrl(string agreementId, string versionId = null, string participantEmail = null, bool? attachSupportingDocuments = null, bool? auditReport = null)
        {
            var documentUrl = await CurrentUser.SignService.GetCombinedDocumentUrlAsync(agreementId, versionId, participantEmail, attachSupportingDocuments, auditReport);

            return documentUrl;
        }

        /// <summary>
        ///     Download a single combined PDF document for the documents associated with an agreement.
        /// </summary>
        /// <param name="agreementId">The agreement identifier.</param>
        /// <param name="versionId">The version identifier.</param>
        /// <param name="participantEmail">The participant email.</param>
        /// <param name="attachSupportingDocuments">The attach supporting documents.</param>
        /// <param name="auditReport">The audit report.</param>
        /// <returns>PDF file stream containing documents associated with an agreement.</returns>
        /// <exception cref="HttpResponseException"></exception>
        /// <exception cref="HttpResponseMessage"></exception>
        /// <exception cref="StringContent"></exception>
        [HttpGet]
        [Route("{agreementId}/combinedDocument/download")]
        public async Task<HttpResponseMessage> DownloadCombinedDocument(string agreementId, string versionId = null, string participantEmail = null, bool? attachSupportingDocuments = null, bool? auditReport = null)
        {
            var stream = await CurrentUser.SignService.DownloadCombinedDocumentAsync(agreementId, versionId, participantEmail, attachSupportingDocuments, auditReport);
            var agreement = await CurrentUser.SignService.GetAgreementInfoAsync(agreementId);
             
            var response = new HttpResponseMessage(HttpStatusCode.OK) { Content = new StreamContent(stream) };
            response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
            response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/pdf");
            response.Content.Headers.ContentDisposition.FileName = $"{agreement.Name}.pdf";

            return response;
        }

        /// <summary>
        ///     Retrieves the history of the agreement.
        /// </summary>
        /// <param name="agreementId">The agreement identifier.</param>
        /// <returns></returns>
        /// <exception cref="HttpResponseException"></exception>
        /// <exception cref="HttpResponseMessage"></exception>
        /// <exception cref="StringContent"></exception>
        [HttpGet]
        [Route("{agreementId}/events")]
        public async Task<List<DocumentHistoryEvent>> GetDocumentHistoryEvents(string agreementId)
        {
            var agreementInfo = await CurrentUser.SignService.GetAgreementInfoAsync(agreementId);
            
            return agreementInfo.Events.ToList();
        }

        /// <summary>
        ///     Download audit trail report.
        /// </summary>
        /// <returns>PDF file stream containing audit trail information</returns>
        /// <exception cref="HttpResponseException"></exception>
        [HttpGet]
        [Route("{agreementId}/auditTrail")]
        public async Task<HttpResponseMessage> DownloadAuditTrail(string agreementId)
        {
            var stream = await CurrentUser.SignService.GetAuditTrailAsync(agreementId);
            var agreement = await CurrentUser.SignService.GetAgreementInfoAsync(agreementId);

            var response = new HttpResponseMessage(HttpStatusCode.OK) { Content = new StreamContent(stream) };
            response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
            response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/pdf");
            response.Content.Headers.ContentDisposition.FileName = $"{agreement.Name}_AuditTrail.pdf";

            return response;
        }

        /// <summary>
        ///     Cancels the agreement.
        /// </summary>
        /// <param name="agreementId">The agreement identifier.</param>
        /// <param name="comment">The comment.</param>
        /// <param name="notifySigner">The notify signer.</param>
        /// <returns>
        ///     AgreementStatusUpdateResponse
        /// </returns>
        /// <exception cref="HttpResponseException"></exception>
        /// <exception cref="HttpResponseMessage"></exception>
        /// <exception cref="StringContent"></exception>
        [HttpPut]
        [Route("{agreementId}/cancel")]
        public AgreementStatusUpdateResponse CancelAgreement(string agreementId, string comment = default(string), bool? notifySigner = default(bool?))
        {
            var agreementStatusUpdateResponse = CurrentUser.SignService.CancelAgreement(agreementId, comment, notifySigner);

            return agreementStatusUpdateResponse;
        }

        /// <summary>
        ///     Deletes the agreement.
        /// </summary>
        /// <param name="agreementId">The agreement identifier.</param>
        /// <returns></returns>
        /// <exception cref="HttpResponseException"></exception>
        /// <exception cref="HttpResponseMessage"></exception>
        /// <exception cref="StringContent"></exception>
        [HttpDelete]
        [Route("{agreementId}/delete")]
        public Task DeleteAgreement(string agreementId)
        {
            CurrentUser.SignService.DeleteAgreement(agreementId);

            return Task.FromResult(0);
        }

        
    }
}