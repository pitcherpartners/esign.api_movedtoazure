﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Microsoft.Rest;
using PitcherPartners.eSign.WebApi.Filters;
using PitcherPartners.eSign.WebApi.Interfaces;
using PitcherPartners.eSign.WebApi.Models.OAuth;
using PitcherPartners.eSign.WebClients.Casu.Models;

namespace PitcherPartners.eSign.WebApi.Controllers
{
    [RoutePrefix("api/oauth")]
    public class OAuthController : BaseApiController
    {
        private readonly IOAuthService _oAuthService;


        /// <summary>
        ///     Inject all required services into the controller
        /// </summary>
        /// <param name="oAuthService"></param>
        public OAuthController(IOAuthService oAuthService)
        {
            _oAuthService = oAuthService;
        }

        /// <summary>
        ///     Verify whether the current logged in user requires OAuth authentication.
        /// </summary>
        /// <returns>Boolean</returns>
        /// <exception cref="HttpResponseException"></exception>
        [HttpGet]
        [Route("requiresAuthentication")]
        public bool RequiresAuthentication(string userName)
        {
            try
            {
                var requiresAuthentication =  _oAuthService.RequiresAuthentication(userName);

                return requiresAuthentication;
            }
            catch (HttpOperationException ex)
            {
                throw new HttpResponseException(
                    new HttpResponseMessage(ex.Response.StatusCode) { Content = new StringContent(ex.Message) });
            }
        }

        /// <summary>
        ///     Returns the authorization url for user to grant access to their account.
        /// </summary>
        /// <returns>Authorization Uri</returns>
        /// <exception cref="HttpResponseException"></exception>
        [HttpGet]
        [Route("authorizationUrl")]
        public Uri GetAuthorizationUrl()
        {
            try
            {
                var scopes = new List<Scope>();
                foreach (var target in Target.All())
                    scopes.Add(new Scope {Target = target, Modifier = Modifier.Self});

                var authorizationRequest = new AuthorizationRequest
                {
                    ClientId = ConfigurationSettings.ClientId,
                    RedirectUri = ConfigurationSettings.OAuthCallbackUrl,
                    Scopes = scopes,
                    ResponseType = "code"
                };

                var authUri = _oAuthService.GetAuthorizationUrl(authorizationRequest);

                return authUri;
            }
            catch (HttpOperationException ex)
            {
                throw new HttpResponseException(
                    new HttpResponseMessage(ex.Response.StatusCode) {Content = new StringContent(ex.Message)});
            }
        }

        /// <summary>
        ///     Get the access token from code supplied and save details to CPD database.
        /// </summary>
        /// <param name="userName">Name of the user.</param>
        /// <param name="code">OAuth authentication code.</param>
        /// <returns>UserExternalAuthenticationEntity</returns>
        /// <exception cref="HttpResponseException"></exception>
        /// <exception cref="HttpResponseMessage"></exception>
        /// <exception cref="StringContent"></exception>
        [HttpPost]
        [Route("processCode")]
        public async Task<UserExternalAuthenticationEntity> ProcessAuthenticationCode(string code, string userName)
        {
            try
            {
                var userExternalAuthenticationEntity =
                    await _oAuthService.ProcessAuthenticationCodeAsync(code, userName);

                return userExternalAuthenticationEntity;
            }
            catch (HttpOperationException ex)
            {
                throw new HttpResponseException(
                    new HttpResponseMessage(ex.Response.StatusCode) {Content = new StringContent(ex.Message)});
            }
        }
    }
}