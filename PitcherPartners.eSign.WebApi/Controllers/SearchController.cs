﻿using PitcherPartners.eSign.WebApi.Filters;
using PitcherPartners.eSign.WebClients.AdobeSign.Models;
using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Microsoft.Rest;

namespace PitcherPartners.eSign.WebApi.Controllers
{
    [AdobeSignAuthenticationFilter]
    [RoutePrefix("api/search")]
    public class SearchController : BaseApiController
    {
        /// <summary>
        /// Create a search object for Search asset event. It will return the result for the first page and search Id to fetch results for further pages.
        /// </summary>
        /// <returns>AgreementAssetEventPostResponse</returns>
        /// <exception cref="HttpResponseException"></exception>
        [HttpPost]
        [Route("createAssetEvent")]
        public async Task<AgreementAssetEventPostResponse> CreateAssetEvent(AgreementAssetEventRequest agreementAssetEventRequest)
        {
            var agreementAssetEventPostResponse = await CurrentUser.SignService.CreateAssetEventAsync(agreementAssetEventRequest);

            return agreementAssetEventPostResponse;

        }

        /// <summary>
        /// Return the result for the page which is described inside the Page Cursor Info.
        /// </summary>
        /// <returns>AgreementAssetEventGetResponse</returns>
        /// <exception cref="HttpResponseException"></exception>
        [HttpGet]
        [Route("searchAssetEvents/{searchId}")]
        public async Task<AgreementAssetEventGetResponse> GetAssetEvent(string searchId, string pageCursor, int? pageSize = null)
        {
            var agreementAssetEventGetResponse = await CurrentUser.SignService.GetAssetEventAsync(searchId, pageCursor, pageSize);

            return agreementAssetEventGetResponse;

        }
    }
}
