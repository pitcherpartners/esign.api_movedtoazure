﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using PitcherPartners.eSign.WebApi.Filters;

namespace PitcherPartners.eSign.WebApi.Controllers
{
    [AdobeSignAuthenticationFilter]
    [RoutePrefix("api/admin")]

    public class AdminController : BaseApiController
    {
        /// <summary>
        ///     FOR DEV ONLY, DO NOT USE IN PRODUCTION! - This endpoint is designed to easily delete all test agreements within current user's account.
        /// </summary>
        /// <returns></returns>
        /// <exception cref="HttpResponseException"></exception>
        /// <exception cref="HttpResponseMessage"></exception>
        /// <exception cref="StringContent"></exception>
        [HttpDelete]
        [Route("deleteAllAgreements")]
        public async Task DeleteAllAgreements()
        {
            var userAgreements = await CurrentUser.SignService.GetAgreementsAsync();
            foreach (var userAgreement in userAgreements.UserAgreementList)
            {
                await CurrentUser.SignService.DeleteAgreement(userAgreement.AgreementId);
            }
        }
    }
}
