﻿using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Mvc;
using PitcherPartners.eSign.WebApi.Configuration;
using PitcherPartners.eSign.WebApi.Filters;
using PitcherPartners.eSign.WebApi.Models;

namespace PitcherPartners.eSign.WebApi.Controllers
{
    [WebApiExceptionFilter]
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class BaseApiController : ApiController
    {
        /// <summary>
        /// Property that gets the current Authenticated ShareFile User 
        /// Authentication occurs on the ShareFileAuthenticationFilter.
        /// </summary>
        public AdobeSignUser CurrentUser
        {
            get
            {
                object userObject = null;
                if (Request.Properties.TryGetValue("AdobeSignUser", out userObject))
                {
                    return userObject as AdobeSignUser;
                }
                return null;
            }
        }

        /// <summary>
        /// Gets the configuration settings.
        /// </summary>
        /// <value>
        /// The configuration settings.
        /// </value>
        public IConfigurationSettings ConfigurationSettings => DependencyResolver.Current.GetService<IConfigurationSettings>();
    }
}
