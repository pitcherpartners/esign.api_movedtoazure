﻿using PitcherPartners.eSign.WebApi.Configuration;
using PitcherPartners.eSign.WebApi.Interfaces;
using PitcherPartners.eSign.WebApi.Models.OAuth;
using PitcherPartners.eSign.WebClients.Casu;
using PitcherPartners.eSign.WebClients.Casu.Models;
using RestSharp;
using System;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace PitcherPartners.eSign.WebApi.Services
{
    /// <summary>
    /// OAuth Manager
    /// </summary>
    public class OAuthService : IOAuthService
    {
        private readonly Uri _oAuthAccessPoint;
        private readonly Uri _webAccessPoint;

        /// <summary>
        /// Initializes a new instance of the <see cref="OAuthService" /> class.
        /// </summary>
        /// <param name="oAuthAccessPoint">The API access point.</param>
        /// <param name="webAccessPoint">The web access point.</param>
        public OAuthService(Uri oAuthAccessPoint, Uri webAccessPoint)
        {
            _oAuthAccessPoint = oAuthAccessPoint;
            _webAccessPoint = webAccessPoint;

            // Adobe Sign server requires high security protocol - this line is required to get around the problem.
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 |
                                                   SecurityProtocolType.Tls;
        }

        /// <summary>
        /// Determines whether the user has an authentication record in the CPD database.
        /// </summary>
        /// <param name="userName">Name of the user.</param>
        /// <returns>
        /// Boolean
        /// </returns>
        /// <exception cref="Exception">Could not find ESIGN application!</exception>
        public bool RequiresAuthentication(string userName)
        {
            bool result = true;

            var configurationSettings = DependencyResolver.Current.GetService<IConfigurationSettings>();

            var casuEndpoint = new Uri(configurationSettings.CasuEndpoint);
            using (var casuApi = new PitcherPartnersCASUWeb(baseUri: casuEndpoint))
            {
                var casuApps = casuApi.Application.Get();
                var eSignApp = casuApps.FirstOrDefault(a => a.IdsApplicationId == "ESIGN");
                if (eSignApp?.ApplicationId == null)
                {
                    throw new Exception("Could not find ESIGN application!");
                }

                var userExternalAuthenticationEntity = casuApi.UserExternalAuthentication.GetByKey(userName, (int)eSignApp.ApplicationId);
                if (userExternalAuthenticationEntity != null) { result = false;}

                return result;
            }
        }

        /// <summary>
        /// Retrieves the authorization url that will be used to get the authorization code.
        /// </summary>
        /// <param name="authorizationRequest">The authorization request.</param>
        /// <returns>The authorization url where the user will be directed to authorize the application.</returns>
        public Uri GetAuthorizationUrl(AuthorizationRequest authorizationRequest)
        {
            const string path = "public/oauth";
            const string redirectUri = "redirect_uri";
            const string responseType = "response_type";
            const string clientId = "client_id";
            const string scope = "scope";
            const string state = "state";

            const string queryStringSeparator = "?";
            const string paramSeparator = "&";
            const string equals = "=";

            var scopes = "";

            foreach (var itemScope in authorizationRequest.Scopes)
            {
                scopes += $"{itemScope.Target}:{itemScope.Modifier} ";
            }

            return new Uri(_webAccessPoint, path + queryStringSeparator +
                                            redirectUri + equals + authorizationRequest.RedirectUri + paramSeparator +
                                            responseType + equals + authorizationRequest.ResponseType + paramSeparator +
                                            clientId + equals + authorizationRequest.ClientId + paramSeparator +
                                            scope + equals + scopes.TrimEnd() +
                                            (authorizationRequest.State != null
                                                ? (paramSeparator + state + equals + authorizationRequest.State)
                                                : ""));
        }

        /// <summary>
        /// Retrieves the access token with the required scopes using the authorization code granted during the authorization.
        /// </summary>
        /// <param name="accessTokenRequest">The access token request.</param>
        /// <returns>
        /// AccessTokenResponse
        /// </returns>
        public AccessTokenResponse GetAccessToken(AccessTokenRequest accessTokenRequest)
        {
            var client = new RestClient(_oAuthAccessPoint);

            var request = new RestRequest("oauth/token", Method.POST);

            if (accessTokenRequest.Code != null)
            {
                request.AddParameter("code", System.Uri.EscapeDataString(accessTokenRequest.Code));
            }
            if (accessTokenRequest.ClientId != null)
            {
                request.AddParameter("client_id", System.Uri.EscapeDataString(accessTokenRequest.ClientId));
            }
            if (accessTokenRequest.ClientSecret != null)
            {
                request.AddParameter("client_secret", System.Uri.EscapeDataString(accessTokenRequest.ClientSecret));
            }
            if (accessTokenRequest.RedirectUri != null)
            {
                request.AddParameter("redirect_uri", accessTokenRequest.RedirectUri);
            }
            if (accessTokenRequest.GrantType != null)
            {
                request.AddParameter("grant_type", System.Uri.EscapeDataString(accessTokenRequest.GrantType));
            }

            request.AddHeader("Content-Type", "application/x-www-form-urlencoded");
            var response = client.Execute<AccessTokenResponse>(request);

            return response.Data;
        }

        /// <summary>
        /// Refreshes the access token.
        /// </summary>
        /// <param name="accessTokenRefreshRequest">The access token refresh request.</param>
        /// <returns>AccessTokenRefreshResponse</returns>
        public AccessTokenRefreshResponse RefreshAccessToken(AccessTokenRefreshRequest accessTokenRefreshRequest)
        {
            var client = new RestClient(_oAuthAccessPoint);

            var request = new RestRequest("oauth/refresh", Method.POST);

            if (accessTokenRefreshRequest.Code != null)
            {
                request.AddParameter("code", System.Uri.EscapeDataString(accessTokenRefreshRequest.Code));
            }
            if (accessTokenRefreshRequest.ClientId != null)
            {
                request.AddParameter("client_id", System.Uri.EscapeDataString(accessTokenRefreshRequest.ClientId));
            }
            if (accessTokenRefreshRequest.ClientSecret != null)
            {
                request.AddParameter("client_secret",
                    System.Uri.EscapeDataString(accessTokenRefreshRequest.ClientSecret));
            }
            if (accessTokenRefreshRequest.RefreshToken != null)
            {
                request.AddParameter("refresh_token",
                    System.Uri.EscapeDataString(accessTokenRefreshRequest.RefreshToken));
            }
            if (accessTokenRefreshRequest.RedirectUri != null)
            {
                request.AddParameter("redirect_uri", accessTokenRefreshRequest.RedirectUri);
            }
            if (accessTokenRefreshRequest.GrantType != null)
            {
                request.AddParameter("grant_type", accessTokenRefreshRequest.GrantType);
            }

            request.AddHeader("Content-Type", "application/x-www-form-urlencoded");

            var response = client.Execute<AccessTokenRefreshResponse>(request);


            return response.Data;
        }

        /// <summary>
        /// Processes the authentication code.
        /// </summary>
        /// <param name="code">The code.</param>
        /// <param name="userName">The username.</param>
        /// <returns>UserExternalAuthenticationEntity</returns>
        /// <exception cref="Exception">Could not find ESIGN application!</exception>
        public async Task<UserExternalAuthenticationEntity> ProcessAuthenticationCodeAsync(string code, string userName)
        {
            var configurationSettings = DependencyResolver.Current.GetService<IConfigurationSettings>();

            var accessTokenRequest = new AccessTokenRequest()
            {
                GrantType = "authorization_code",
                ClientId = configurationSettings.ClientId,
                ClientSecret = configurationSettings.ClientSecret,
                RedirectUri = configurationSettings.OAuthRedirectUrl,
                Code = code
            };

            var token = this.GetAccessToken(accessTokenRequest);

            if (token?.AccessToken == null)
            {
                throw new Exception("Token is empty");
            }

            var casuEndpoint = new Uri(configurationSettings.CasuEndpoint);
            using (var casuApi = new PitcherPartnersCASUWeb(baseUri: casuEndpoint))
            {
                var casuApps = casuApi.Application.Get();
                var eSignApp = casuApps.FirstOrDefault(a => a.IdsApplicationId == "ESIGN");
                if (eSignApp == null)
                {
                    throw new Exception("Could not find ESIGN application!");
                }

                var userExternalAuthenticationEntity = new UserExternalAuthenticationEntity()
                {
                    Code = code,
                    AccessToken = token.AccessToken,
                    //There's a bug in Adobe Sign, it always returns the refresh token with an asterisk
                    RefreshToken = token.RefreshToken.Replace("*", ""),
                    ApplicationId = eSignApp.ApplicationId,
                    UserName = userName,
                    LastUpdateDateTime = DateTime.Now,
                    ExpirationDateTime = DateTime.Now.AddSeconds(token.ExpiresIn)
                };

                var result = await casuApi.UserExternalAuthentication.PostAsync(userExternalAuthenticationEntity);

                return result;
            }
        }
    }
}