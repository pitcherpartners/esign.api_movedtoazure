﻿using System;
using PitcherPartners.eSign.WebApi.Configuration;
using PitcherPartners.eSign.WebApi.Interfaces;
using PitcherPartners.eSign.WebClients.AdobeSign;
using PitcherPartners.eSign.WebClients.AdobeSign.Models;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Threading.Tasks;
using PitcherPartners.eSign.WebClients.Casu;

namespace PitcherPartners.eSign.WebApi.Services
{
    public class SignService : ISignService
    {
        private readonly IAdobeSignWebApiProxy _adobeSignClient;
        private string _xApiUser;

        public SignService(IAdobeSignWebApiProxy adobeSignClient)
        {
            _adobeSignClient = adobeSignClient;

            // Adobe Sign server requires high security protocol - this line is required to get around the problem.
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;
        }

        public string AccessToken { get; set; }

        public string XApiUser
        {
            get
            {
                if (_xApiUser == null)
                {
                    return null;
                }
                else
                {
                    return $"email:{_xApiUser}";
                }
            }
            set => _xApiUser = value;
        }

        public void SetCredentials(string accessToken, string xApiUser)
        {
            AccessToken = accessToken;
            XApiUser = xApiUser;
        }

        #region "Agreements"
        public async Task<AgreementCreationResponse> CreateAgreementAsync(AgreementCreationInfo agreementCreationInfo)
        {
            var agreementCreationResponse = await _adobeSignClient.CreateAgreementAsync(AccessToken, agreementCreationInfo, XApiUser);

            return agreementCreationResponse;
        }
        public async Task<TransientDocumentResponse> CreateTransientDocumentAsync(System.IO.Stream fileStream, string fileName = default(string), string mimeType = default(string))
        {
            var transientDocumentResponse = await _adobeSignClient.CreateTransientDocumentAsync(AccessToken, fileStream, XApiUser, fileName, mimeType);
            if (transientDocumentResponse.TransientDocumentId.EndsWith("*"))
            {
                char[] charsToTrim = {'*'};
                transientDocumentResponse.TransientDocumentId =
                    transientDocumentResponse.TransientDocumentId.TrimEnd(charsToTrim);
            }
                    
            return transientDocumentResponse;
        }
        public async Task<AgreementInfo> GetAgreementInfoAsync(string agreementId)
        {
            var agreementInfo = await _adobeSignClient.GetAgreementInfoAsync(AccessToken, agreementId, XApiUser);

            return agreementInfo;
        }
        public async Task<UserAgreements> GetAgreementsAsync(string query = default(string), string externalId = default(string), string externalGroup = default(string), string externalNamespace = default(string))
        {
            var userAgreements = await _adobeSignClient.GetAgreementsAsync(AccessToken, XApiUser, query, externalId, externalGroup, externalNamespace);

            return userAgreements;
        }
        public async Task<AgreementDocuments> GetAgreementDocumentsAsync(string agreementId, string versionId = null, string participantEmail = null, string supportingDocumentContentFormat = null)
        {
            var agreementDocuments = await _adobeSignClient.GetAllDocumentsAsync(AccessToken, agreementId, XApiUser, versionId, participantEmail, supportingDocumentContentFormat);

            return agreementDocuments;
        }
        public async Task<Stream> GetAuditTrailAsync(string agreementId)
        {
            var auditTrailBytes = await _adobeSignClient.GetAuditTrailAsync(AccessToken, agreementId, XApiUser);

            return auditTrailBytes;
        }
        public async Task<Stream> DownloadCombinedDocumentAsync(string agreementId, string versionId = null, string participantEmail = null, bool? attachSupportingDocuments = null, bool? auditReport = null)
        {
            var combinedDocumentBytes = await _adobeSignClient.GetCombinedDocumentAsync(AccessToken, agreementId, XApiUser, versionId, participantEmail, attachSupportingDocuments, auditReport);

            return combinedDocumentBytes;
        }
        public async Task<DocumentImageUrls> GetCombinedDocumentImageUrlsAsync(string agreementId, string versionId = default(string), string participantEmail = default(string), string imageSizes = default(string), bool? includeSupportingDocumentsImageUrls = default(bool?), bool? showImageAvailabilityOnly = default(bool?))
        {
            var imageUrls = await _adobeSignClient.GetCombinedDocumentImageUrlsAsync(AccessToken, agreementId, XApiUser, versionId, participantEmail, imageSizes, includeSupportingDocumentsImageUrls, showImageAvailabilityOnly);

            return imageUrls;
        }
        public async Task<DocumentUrl> GetCombinedDocumentUrlAsync(string agreementId, string versionId = null, string participantEmail = null, bool? attachSupportingDocuments = null, bool? auditReport = null)
        {
            var documentUrl = await _adobeSignClient.GetCombinedDocumentUrlAsync(AccessToken, agreementId, XApiUser, versionId, participantEmail, attachSupportingDocuments, auditReport);

            return documentUrl;
        }
        public async Task<DocumentImageUrl> GetDocumentImageUrlsAsync(string agreementId, string documentId, string versionId = null, string participantEmail = null, string imageSizes = null, bool? showImageAvailabilityOnly = null, int? startPage = null, int? endPage = null)
        {
            var documentImageUrl = await _adobeSignClient.GetDocumentImageUrlsAsync(AccessToken, agreementId, documentId, XApiUser, versionId, participantEmail, imageSizes, showImageAvailabilityOnly, startPage, endPage);

            return documentImageUrl;
        }
        public AgreementStatusUpdateResponse CancelAgreement(string agreementId, string comment = default(string), bool? notifySigner = default(bool?))
        {
            AgreementStatusUpdateInfo agreementStatusUpdateInfo = new AgreementStatusUpdateInfo()
            {
                Comment = comment,
                NotifySigner = notifySigner
            };

            var agreementStatusUpdateResponse = _adobeSignClient.UpdateStatus(AccessToken, agreementId, agreementStatusUpdateInfo, XApiUser);

            return agreementStatusUpdateResponse;
        }
        public Task DeleteAgreement(string agreementId)
        {
            _adobeSignClient.DeleteAgreement(AccessToken, agreementId, XApiUser);

            return Task.FromResult(0);
        }
        #endregion  

        #region "Search"
        public async Task<AgreementAssetEventPostResponse> CreateAssetEventAsync(AgreementAssetEventRequest agreementAssetEventRequest)
        {
            var agreementAssetEventPostResponse = await _adobeSignClient.CreateAssetEventAsync(AccessToken, agreementAssetEventRequest, XApiUser);

            return agreementAssetEventPostResponse;
        }
        public async Task<AgreementAssetEventGetResponse> GetAssetEventAsync(string searchId, string pageCursor, int? pageSize = null)
        {
            var agreementAssetEventGetResponse = await _adobeSignClient.GetAssetEventAsync(AccessToken, searchId, pageCursor, XApiUser, pageSize);

            return agreementAssetEventGetResponse;
        }
        #endregion

        #region "Reminders"
        public async Task<ReminderCreationResult> CreateReminderAsync(ReminderCreationInfo reminderCreationInfo)
        {
            var reminderCreationResult = await _adobeSignClient.CreateReminderAsync(AccessToken, reminderCreationInfo, XApiUser);

            return reminderCreationResult;
        }
        #endregion

        #region "Users"
        public async Task<UsersInfo> GetUsersAsync(string email = default(string))
        {
            var usersInfo = await _adobeSignClient.GetUsersAsync(AccessToken,XApiUser, email);

            return usersInfo;
        }
        public async Task<UserDetailsInfo> GetUserDetailAsync(string userId)
        {
            var userDetailsInfo = await _adobeSignClient.GetUserDetailAsync(AccessToken,userId, XApiUser);

            return userDetailsInfo;
        }

        #endregion

    }
}