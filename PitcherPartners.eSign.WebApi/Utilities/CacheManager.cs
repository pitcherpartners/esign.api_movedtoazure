﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Caching;
using System.Threading.Tasks;
using System.Web;

namespace PitcherPartners.eSign.WebApi.Utilities
{
    /// <summary>
    /// Cache Management
    /// </summary>
    public class CacheManager
    {
        /// <summary>
        /// Default cache item policy for every controller
        /// </summary>
        public CacheItemPolicy DefaultCachePolicy { get; set; }

        /// <summary>
        /// Generic method to get data from cache, includes an option to read data in case of cache-miss and cache retrieved result with non-default expiration policy.
        /// Also can be used to invalidate item in the cache.
        /// </summary>
        /// <typeparam name="TCached">Type of object to return.</typeparam>
        /// <param name="memCache">instance of cache to work on</param>
        /// <param name="cacheKey">Cache key.</param>
        /// <param name="invalidateCache">flag to invalidate cache explicitly - so dataReaderFunc will be called or method returns null</param>
        /// <param name="dataReaderFunc">Function to read data in case of cache miss</param>
        /// <param name="cacheItemPolicy">Cache item policy if differs from default one.</param>
        /// <returns>Promise of cached object.</returns>
        public async Task<TCached> GetFromCache<TCached>(MemoryCache memCache, string cacheKey, bool invalidateCache = false, Func<Task<TCached>> dataReaderFunc = null, CacheItemPolicy cacheItemPolicy = null)
        {
            var key = cacheKey.ToLowerInvariant();

            //invalidate entry in the cache explicitly if requested
            if (invalidateCache) { CleanCacheEntry(memCache, key); }

            // if object is in cache - return cached version
            if (memCache.Contains(key)) { return (TCached)memCache.Get(key); }

            // Not in cache and no function to get data - return default for the type.
            if (dataReaderFunc == null) { return default(TCached); }

            // Fetch data from function
            var data = await dataReaderFunc();

            //Cache received item with the policy (explicit or default)
            memCache.Add(new CacheItem(key, data), cacheItemPolicy ?? DefaultCachePolicy);

            return (TCached)memCache.Get(key);
        }


        /// <summary>
        /// Set item to cache
        /// </summary>
        /// <param name="memCache"></param>
        /// <param name="cacheKey"></param>
        /// <param name="data"></param>
        /// <param name="cacheItemPolicy"></param>
        /// <typeparam name="TCached"></typeparam>
        public void SetToCache<TCached>(MemoryCache memCache, string cacheKey, TCached data, CacheItemPolicy cacheItemPolicy = null)
        {
            var key = cacheKey.ToLowerInvariant();
            memCache.Set(key, data, cacheItemPolicy ?? DefaultCachePolicy);
        }

        /// <summary>
        /// Cleanup entry from cache by it's key
        /// </summary>
        /// <param name="memCache">instance of cache to work on</param>
        /// <param name="cacheKey">Cache key.</param>
        public void CleanCacheEntry(MemoryCache memCache, string cacheKey)
        {
            var key = cacheKey.ToLowerInvariant();
            if (memCache.Contains(key)) { memCache.Remove(key); }
        }
    }
}