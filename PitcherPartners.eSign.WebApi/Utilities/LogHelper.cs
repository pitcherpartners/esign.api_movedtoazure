﻿using PitcherPartners.Logging;
using System;
using System.Diagnostics;
using System.Net;
using System.Runtime.CompilerServices;
using System.Text;

namespace PitcherPartners.eSign.WebApi.Utilities
{
    /// <summary>
    /// A utility to log exceptions into the Logging database
    /// </summary>
    public static class LogHelper
    {
        /// <summary>
        /// Method to write to the log from a http action
        /// </summary>
        public static void WriteHttpLog(TraceEventType eventType, HttpStatusCode statusCode, string message = "",
            Exception exception = null, [CallerFilePath] string filePath = "",
            [CallerMemberName] string memberName = "", [CallerLineNumber] int callerLineNumber = 0)
        {

            var customdata = string.Empty;

            if (exception != null)
            {
                var sb = new StringBuilder();
                sb.Append("Exception: " + exception.ToString());
                if (exception.InnerException != null)
                {
                    sb.Append(" Inner Exception: " + exception.InnerException.ToString());
                }

                customdata = sb.ToString();
            }

            var logMessage =
                $"Path: {filePath}, Method: {memberName}, Line Number: {callerLineNumber}, Status Code: {statusCode}, Message: {message.Trim()}";

            var logEntry = new CustomLogEntry()
            {
                Categories = new string[] {"General"},
                Message = logMessage,
                Severity = eventType,
                CustomData = customdata
            };

            logEntry.Write();

        }

        /// <summary>
        /// Method to write to the log from business logic action
        /// </summary>
        public static void WriteLog(TraceEventType eventType, string message = "", Exception exception = null,
            [CallerFilePath] string filePath = "", [CallerMemberName] string memberName = "",
            [CallerLineNumber] int callerLineNumber = 0)
        {

            var customdata = string.Empty;

            if (exception != null)
            {
                var sb = new StringBuilder();
                sb.Append("Exception: " + exception.ToString());
                if (exception.InnerException != null)
                {
                    sb.Append(" Inner Exception: " + exception.InnerException.ToString());
                }

                customdata = sb.ToString();
            }

            var logMessage = !string.IsNullOrEmpty(message)
                ? $"Path: {filePath},  Method: {memberName}, Line Number: {callerLineNumber}, Message: {message}"
                : $"Path: {filePath},  Method: {memberName}, Line Number: {callerLineNumber}";

            var logEntry = new CustomLogEntry()
            {
                Categories = new string[] {"General"},
                Message = logMessage,
                Severity = eventType,
                CustomData = customdata
            };

            logEntry.Write();

        }

    }
}