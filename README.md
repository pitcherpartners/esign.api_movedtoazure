eSign Api Readme
What is this repository for?
Quick summary

The eSign Api is a .NET Web API 2 solution that wraps all the logic regarding Adobe Sign functionality (using Adobe Sign API), that is:

Create agreements
Delete Agreements
List Agreements under an user account
Search for agreements and assets and so on
To perform these operations it uses the Adobe Sign RESTful Web Api Proxy generated from their website's Swagger Json file.

How do I get set up?
Summary of set up

To debug this Web Api simply run in from Visual Studio, the authentication is using OAuth to get a user access token from Adobe's authentication endpoint.

Configuration

The main configuration parameters are related to the Adobe Sign API access. These parameters are transformed to production values using transformations.
        <add key="ShareFileApiBaseUrl" value="https://secure.sf-api.com/sf/v3/" />

Dependencies

Adobe Sign RESTful services 

Database configuration

The Adobe Sign related tables are within the CPD database.


How to run tests

Currently there are no automated tests configured.

Deployment instructions

eSign is configured in Bamboo to automatically build and deploy any changes committed into the Test branch to the Test environment: esign-tst.mypitcher.com.au/swagger

Swagger
Swagger Documentation
Contribution guidelines
Writing tests
Code review
Other guidelines
Who do I talk to?
Repo owner or admin
Other community or team contact