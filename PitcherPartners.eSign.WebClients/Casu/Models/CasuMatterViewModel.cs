// Code generated by Microsoft (R) AutoRest Code Generator 1.2.2.0
// Changes may cause incorrect behavior and will be lost if the code is
// regenerated.

namespace PitcherPartners.eSign.WebClients.Casu.Models
{
    using Newtonsoft.Json;
    using PitcherPartners.eSign;
    using PitcherPartners.eSign.WebClients;
    using PitcherPartners.eSign.WebClients.Casu;
    using System.Linq;

    public partial class CasuMatterViewModel
    {
        /// <summary>
        /// Initializes a new instance of the CasuMatterViewModel class.
        /// </summary>
        public CasuMatterViewModel()
        {
          CustomInit();
        }

        /// <summary>
        /// Initializes a new instance of the CasuMatterViewModel class.
        /// </summary>
        public CasuMatterViewModel(string entityId = default(string), string matterId = default(string), string matterName = default(string), string matterType = default(string), string matterDepartment = default(string), string serviceName = default(string), string matterManagerId = default(string), string matterManagerName = default(string), string matterManagerEmail = default(string), string matterManagerAdUsername = default(string))
        {
            EntityId = entityId;
            MatterId = matterId;
            MatterName = matterName;
            MatterType = matterType;
            MatterDepartment = matterDepartment;
            ServiceName = serviceName;
            MatterManagerId = matterManagerId;
            MatterManagerName = matterManagerName;
            MatterManagerEmail = matterManagerEmail;
            MatterManagerAdUsername = matterManagerAdUsername;
            CustomInit();
        }

        /// <summary>
        /// An initialization method that performs custom operations like setting defaults
        /// </summary>
        partial void CustomInit();

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "entityId")]
        public string EntityId { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "matterId")]
        public string MatterId { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "matterName")]
        public string MatterName { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "matterType")]
        public string MatterType { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "matterDepartment")]
        public string MatterDepartment { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "serviceName")]
        public string ServiceName { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "matterManagerId")]
        public string MatterManagerId { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "matterManagerName")]
        public string MatterManagerName { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "matterManagerEmail")]
        public string MatterManagerEmail { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "matterManagerAdUsername")]
        public string MatterManagerAdUsername { get; set; }

    }
}
