// Code generated by Microsoft (R) AutoRest Code Generator 1.2.2.0
// Changes may cause incorrect behavior and will be lost if the code is
// regenerated.

namespace PitcherPartners.eSign.WebClients.Casu.Models
{
    using Newtonsoft.Json;
    using PitcherPartners.eSign;
    using PitcherPartners.eSign.WebClients;
    using PitcherPartners.eSign.WebClients.Casu;
    using System.Linq;

    public partial class CasuEntityViewModel
    {
        /// <summary>
        /// Initializes a new instance of the CasuEntityViewModel class.
        /// </summary>
        public CasuEntityViewModel()
        {
          CustomInit();
        }

        /// <summary>
        /// Initializes a new instance of the CasuEntityViewModel class.
        /// </summary>
        public CasuEntityViewModel(string abn = default(string), string acn = default(string), string entityManagerId = default(string), string entityManagerName = default(string), string entityPartnerId = default(string), string entityPartnerName = default(string), string entityId = default(string), string entityType = default(string), string name = default(string), string tfn = default(string), bool? topLevelFlag = default(bool?), bool? existing = default(bool?))
        {
            Abn = abn;
            Acn = acn;
            EntityManagerId = entityManagerId;
            EntityManagerName = entityManagerName;
            EntityPartnerId = entityPartnerId;
            EntityPartnerName = entityPartnerName;
            EntityId = entityId;
            EntityType = entityType;
            Name = name;
            Tfn = tfn;
            TopLevelFlag = topLevelFlag;
            Existing = existing;
            CustomInit();
        }

        /// <summary>
        /// An initialization method that performs custom operations like setting defaults
        /// </summary>
        partial void CustomInit();

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "abn")]
        public string Abn { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "acn")]
        public string Acn { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "entityManagerId")]
        public string EntityManagerId { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "entityManagerName")]
        public string EntityManagerName { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "entityPartnerId")]
        public string EntityPartnerId { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "entityPartnerName")]
        public string EntityPartnerName { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "entityId")]
        public string EntityId { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "entityType")]
        public string EntityType { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "tfn")]
        public string Tfn { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "topLevelFlag")]
        public bool? TopLevelFlag { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "existing")]
        public bool? Existing { get; set; }

    }
}
