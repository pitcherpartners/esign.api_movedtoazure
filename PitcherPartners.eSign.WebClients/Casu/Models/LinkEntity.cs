// Code generated by Microsoft (R) AutoRest Code Generator 1.2.2.0
// Changes may cause incorrect behavior and will be lost if the code is
// regenerated.

namespace PitcherPartners.eSign.WebClients.Casu.Models
{
    using Microsoft.Rest;
    using Newtonsoft.Json;
    using PitcherPartners.eSign;
    using PitcherPartners.eSign.WebClients;
    using PitcherPartners.eSign.WebClients.Casu;
    using System.Linq;

    public partial class LinkEntity
    {
        /// <summary>
        /// Initializes a new instance of the LinkEntity class.
        /// </summary>
        public LinkEntity()
        {
          CustomInit();
        }

        /// <summary>
        /// Initializes a new instance of the LinkEntity class.
        /// </summary>
        public LinkEntity(string linkName, string url, int? linkId = default(int?), string imageSmall = default(string), string imageMedium = default(string), string imageLarge = default(string), bool? selected = default(bool?))
        {
            LinkId = linkId;
            LinkName = linkName;
            Url = url;
            ImageSmall = imageSmall;
            ImageMedium = imageMedium;
            ImageLarge = imageLarge;
            Selected = selected;
            CustomInit();
        }

        /// <summary>
        /// An initialization method that performs custom operations like setting defaults
        /// </summary>
        partial void CustomInit();

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "linkId")]
        public int? LinkId { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "linkName")]
        public string LinkName { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "url")]
        public string Url { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "imageSmall")]
        public string ImageSmall { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "imageMedium")]
        public string ImageMedium { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "imageLarge")]
        public string ImageLarge { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "selected")]
        public bool? Selected { get; set; }

        /// <summary>
        /// Validate the object.
        /// </summary>
        /// <exception cref="ValidationException">
        /// Thrown if validation fails
        /// </exception>
        public virtual void Validate()
        {
            if (LinkName == null)
            {
                throw new ValidationException(ValidationRules.CannotBeNull, "LinkName");
            }
            if (Url == null)
            {
                throw new ValidationException(ValidationRules.CannotBeNull, "Url");
            }
        }
    }
}
