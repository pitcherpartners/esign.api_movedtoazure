// Code generated by Microsoft (R) AutoRest Code Generator 1.2.2.0
// Changes may cause incorrect behavior and will be lost if the code is
// regenerated.

namespace PitcherPartners.eSign.WebClients.Casu.Models
{
    using Newtonsoft.Json;
    using PitcherPartners.eSign;
    using PitcherPartners.eSign.WebClients;
    using PitcherPartners.eSign.WebClients.Casu;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;

    public partial class CasuUserAccessViewModel
    {
        /// <summary>
        /// Initializes a new instance of the CasuUserAccessViewModel class.
        /// </summary>
        public CasuUserAccessViewModel()
        {
          CustomInit();
        }

        /// <summary>
        /// Initializes a new instance of the CasuUserAccessViewModel class.
        /// </summary>
        public CasuUserAccessViewModel(int? id = default(int?), string username = default(string), string idsId = default(string), string email = default(string), string firstname = default(string), string lastname = default(string), string password = default(string), IList<CasuAccessViewModel> accessList = default(IList<CasuAccessViewModel>), bool? sendEmailOnSave = default(bool?), bool? sendRequesterEmailOnSave = default(bool?), bool? disabled = default(bool?), string requester = default(string), string requesterEmail = default(string), string requesterGivenName = default(string), string requesterSurname = default(string), string lastTandC = default(string), System.DateTime? lastTandCAcceptDateTime = default(System.DateTime?))
        {
            Id = id;
            Username = username;
            IdsId = idsId;
            Email = email;
            Firstname = firstname;
            Lastname = lastname;
            Password = password;
            AccessList = accessList;
            SendEmailOnSave = sendEmailOnSave;
            SendRequesterEmailOnSave = sendRequesterEmailOnSave;
            Disabled = disabled;
            Requester = requester;
            RequesterEmail = requesterEmail;
            RequesterGivenName = requesterGivenName;
            RequesterSurname = requesterSurname;
            LastTandC = lastTandC;
            LastTandCAcceptDateTime = lastTandCAcceptDateTime;
            CustomInit();
        }

        /// <summary>
        /// An initialization method that performs custom operations like setting defaults
        /// </summary>
        partial void CustomInit();

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "id")]
        public int? Id { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "username")]
        public string Username { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "idsId")]
        public string IdsId { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "email")]
        public string Email { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "firstname")]
        public string Firstname { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "lastname")]
        public string Lastname { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "password")]
        public string Password { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "accessList")]
        public IList<CasuAccessViewModel> AccessList { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "sendEmailOnSave")]
        public bool? SendEmailOnSave { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "sendRequesterEmailOnSave")]
        public bool? SendRequesterEmailOnSave { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "disabled")]
        public bool? Disabled { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "requester")]
        public string Requester { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "requesterEmail")]
        public string RequesterEmail { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "requesterGivenName")]
        public string RequesterGivenName { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "requesterSurname")]
        public string RequesterSurname { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "lastTandC")]
        public string LastTandC { get; set; }

        /// <summary>
        /// </summary>
        [JsonProperty(PropertyName = "lastTandCAcceptDateTime")]
        public System.DateTime? LastTandCAcceptDateTime { get; set; }

    }
}
