// Code generated by Microsoft (R) AutoRest Code Generator 1.2.2.0
// Changes may cause incorrect behavior and will be lost if the code is
// regenerated.

namespace PitcherPartners.eSign.WebClients.Casu.Models
{
    using Newtonsoft.Json;
    using PitcherPartners.eSign;
    using PitcherPartners.eSign.WebClients;
    using PitcherPartners.eSign.WebClients.Casu;
    using System.Linq;

    public partial class MyPitcherContactModel
    {
        /// <summary>
        /// Initializes a new instance of the MyPitcherContactModel class.
        /// </summary>
        public MyPitcherContactModel()
        {
          CustomInit();
        }

        /// <summary>
        /// Initializes a new instance of the MyPitcherContactModel class.
        /// </summary>
        /// <param name="name">Contact name</param>
        /// <param name="contactPhoto">Contact image</param>
        /// <param name="phoneNumber">Contact phone number</param>
        /// <param name="email">Contact email</param>
        public MyPitcherContactModel(string name = default(string), string contactPhoto = default(string), string phoneNumber = default(string), string email = default(string))
        {
            Name = name;
            ContactPhoto = contactPhoto;
            PhoneNumber = phoneNumber;
            Email = email;
            CustomInit();
        }

        /// <summary>
        /// An initialization method that performs custom operations like setting defaults
        /// </summary>
        partial void CustomInit();

        /// <summary>
        /// Gets or sets contact name
        /// </summary>
        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets contact image
        /// </summary>
        [JsonProperty(PropertyName = "contactPhoto")]
        public string ContactPhoto { get; set; }

        /// <summary>
        /// Gets or sets contact phone number
        /// </summary>
        [JsonProperty(PropertyName = "phoneNumber")]
        public string PhoneNumber { get; set; }

        /// <summary>
        /// Gets or sets contact email
        /// </summary>
        [JsonProperty(PropertyName = "email")]
        public string Email { get; set; }

    }
}
