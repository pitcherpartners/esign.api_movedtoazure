// Code generated by Microsoft (R) AutoRest Code Generator 1.2.2.0
// Changes may cause incorrect behavior and will be lost if the code is
// regenerated.

namespace PitcherPartners.eSign.WebClients.Casu.Models
{
    using Newtonsoft.Json;
    using PitcherPartners.eSign;
    using PitcherPartners.eSign.WebClients;
    using PitcherPartners.eSign.WebClients.Casu;
    using System.Linq;

    /// <summary>
    /// Services for Entity in MyPitcher
    /// </summary>
    public partial class EntityApplicationModel
    {
        /// <summary>
        /// Initializes a new instance of the EntityApplicationModel class.
        /// </summary>
        public EntityApplicationModel()
        {
          CustomInit();
        }

        /// <summary>
        /// Initializes a new instance of the EntityApplicationModel class.
        /// </summary>
        /// <param name="id">Application Id</param>
        /// <param name="name">Application Name</param>
        /// <param name="url">Application URL, or the data to generate
        /// corresponding url on the caller side</param>
        /// <param name="appImage">Base-64 encoded image of the application or
        /// empty string if none</param>
        public EntityApplicationModel(string id = default(string), string name = default(string), string url = default(string), string appImage = default(string))
        {
            Id = id;
            Name = name;
            Url = url;
            AppImage = appImage;
            CustomInit();
        }

        /// <summary>
        /// An initialization method that performs custom operations like setting defaults
        /// </summary>
        partial void CustomInit();

        /// <summary>
        /// Gets or sets application Id
        /// </summary>
        [JsonProperty(PropertyName = "id")]
        public string Id { get; set; }

        /// <summary>
        /// Gets or sets application Name
        /// </summary>
        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets application URL, or the data to generate corresponding
        /// url on the caller side
        /// </summary>
        [JsonProperty(PropertyName = "url")]
        public string Url { get; set; }

        /// <summary>
        /// Gets or sets base-64 encoded image of the application or empty
        /// string if none
        /// </summary>
        [JsonProperty(PropertyName = "appImage")]
        public string AppImage { get; set; }

    }
}
