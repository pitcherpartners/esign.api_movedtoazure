// Code generated by Microsoft (R) AutoRest Code Generator 1.2.2.0
// Changes may cause incorrect behavior and will be lost if the code is
// regenerated.

namespace PitcherPartners.eSign.WebClients.AdobeSign.Models
{
    using Newtonsoft.Json;
    using PitcherPartners.eSign;
    using PitcherPartners.eSign.WebClients;
    using PitcherPartners.eSign.WebClients.AdobeSign;
    using System.Linq;

    public partial class FileUploadOptions
    {
        /// <summary>
        /// Initializes a new instance of the FileUploadOptions class.
        /// </summary>
        public FileUploadOptions()
        {
          CustomInit();
        }

        /// <summary>
        /// Initializes a new instance of the FileUploadOptions class.
        /// </summary>
        /// <param name="libraryDocument">Whether library documents link should
        /// appear or not. Default value is taken as true</param>
        /// <param name="localFile">Whether local file upload button should
        /// appear or not. Default value is taken as true</param>
        /// <param name="webConnectors">Whether link to attach documents from
        /// web sources like Dropbox should appear or not. Default value is
        /// taken as true</param>
        public FileUploadOptions(bool? libraryDocument = default(bool?), bool? localFile = default(bool?), bool? webConnectors = default(bool?))
        {
            LibraryDocument = libraryDocument;
            LocalFile = localFile;
            WebConnectors = webConnectors;
            CustomInit();
        }

        /// <summary>
        /// An initialization method that performs custom operations like setting defaults
        /// </summary>
        partial void CustomInit();

        /// <summary>
        /// Gets or sets whether library documents link should appear or not.
        /// Default value is taken as true
        /// </summary>
        [JsonProperty(PropertyName = "libraryDocument")]
        public bool? LibraryDocument { get; set; }

        /// <summary>
        /// Gets or sets whether local file upload button should appear or not.
        /// Default value is taken as true
        /// </summary>
        [JsonProperty(PropertyName = "localFile")]
        public bool? LocalFile { get; set; }

        /// <summary>
        /// Gets or sets whether link to attach documents from web sources like
        /// Dropbox should appear or not. Default value is taken as true
        /// </summary>
        [JsonProperty(PropertyName = "webConnectors")]
        public bool? WebConnectors { get; set; }

    }
}
