// Code generated by Microsoft (R) AutoRest Code Generator 1.2.2.0
// Changes may cause incorrect behavior and will be lost if the code is
// regenerated.

namespace PitcherPartners.eSign.WebClients.AdobeSign.Models
{
    using Microsoft.Rest;
    using Newtonsoft.Json;
    using PitcherPartners.eSign;
    using PitcherPartners.eSign.WebClients;
    using PitcherPartners.eSign.WebClients.AdobeSign;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;

    public partial class AgreementAssetEventPostResponse
    {
        /// <summary>
        /// Initializes a new instance of the AgreementAssetEventPostResponse
        /// class.
        /// </summary>
        public AgreementAssetEventPostResponse()
        {
          CustomInit();
        }

        /// <summary>
        /// Initializes a new instance of the AgreementAssetEventPostResponse
        /// class.
        /// </summary>
        /// <param name="currentPageCursor">The value of the page cursor
        /// corresponding to the current page</param>
        /// <param name="events">An ordered list of the events in the audit
        /// trail of this document</param>
        /// <param name="nextPageCursor">The page cursor of the next page to be
        /// fetched. If the next page cursor is blank then the given page is
        /// the last page</param>
        /// <param name="searchId">The search Id corresponding to current
        /// search object. This searchId can be used in combination with
        /// pageCursors retrieved from the API which retrieves
        /// agreementAssetEvents based on a searchId, to fetch the result for
        /// further pages</param>
        public AgreementAssetEventPostResponse(string currentPageCursor, IList<DocumentEventForUser> events, string nextPageCursor, string searchId)
        {
            CurrentPageCursor = currentPageCursor;
            Events = events;
            NextPageCursor = nextPageCursor;
            SearchId = searchId;
            CustomInit();
        }

        /// <summary>
        /// An initialization method that performs custom operations like setting defaults
        /// </summary>
        partial void CustomInit();

        /// <summary>
        /// Gets or sets the value of the page cursor corresponding to the
        /// current page
        /// </summary>
        [JsonProperty(PropertyName = "currentPageCursor")]
        public string CurrentPageCursor { get; set; }

        /// <summary>
        /// Gets or sets an ordered list of the events in the audit trail of
        /// this document
        /// </summary>
        [JsonProperty(PropertyName = "events")]
        public IList<DocumentEventForUser> Events { get; set; }

        /// <summary>
        /// Gets or sets the page cursor of the next page to be fetched. If the
        /// next page cursor is blank then the given page is the last page
        /// </summary>
        [JsonProperty(PropertyName = "nextPageCursor")]
        public string NextPageCursor { get; set; }

        /// <summary>
        /// Gets or sets the search Id corresponding to current search object.
        /// This searchId can be used in combination with pageCursors retrieved
        /// from the API which retrieves agreementAssetEvents based on a
        /// searchId, to fetch the result for further pages
        /// </summary>
        [JsonProperty(PropertyName = "searchId")]
        public string SearchId { get; set; }

        /// <summary>
        /// Validate the object.
        /// </summary>
        /// <exception cref="ValidationException">
        /// Thrown if validation fails
        /// </exception>
        public virtual void Validate()
        {
            if (CurrentPageCursor == null)
            {
                throw new ValidationException(ValidationRules.CannotBeNull, "CurrentPageCursor");
            }
            if (Events == null)
            {
                throw new ValidationException(ValidationRules.CannotBeNull, "Events");
            }
            if (NextPageCursor == null)
            {
                throw new ValidationException(ValidationRules.CannotBeNull, "NextPageCursor");
            }
            if (SearchId == null)
            {
                throw new ValidationException(ValidationRules.CannotBeNull, "SearchId");
            }
            if (Events != null)
            {
                foreach (var element in Events)
                {
                    if (element != null)
                    {
                        element.Validate();
                    }
                }
            }
        }
    }
}
