// Code generated by Microsoft (R) AutoRest Code Generator 1.2.2.0
// Changes may cause incorrect behavior and will be lost if the code is
// regenerated.

namespace PitcherPartners.eSign.WebClients.AdobeSign.Models
{
    using Microsoft.Rest;
    using Newtonsoft.Json;
    using PitcherPartners.eSign;
    using PitcherPartners.eSign.WebClients;
    using PitcherPartners.eSign.WebClients.AdobeSign;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;

    public partial class CounterSignerSetInfo
    {
        /// <summary>
        /// Initializes a new instance of the CounterSignerSetInfo class.
        /// </summary>
        public CounterSignerSetInfo()
        {
          CustomInit();
        }

        /// <summary>
        /// Initializes a new instance of the CounterSignerSetInfo class.
        /// </summary>
        /// <param name="counterSignerSetMemberInfos">Information about the
        /// members of the counter signer set, currently we support only one
        /// member</param>
        /// <param name="counterSignerSetRole">Specify the role of counter
        /// signer set. Possible values include: 'SIGNER', 'APPROVER',
        /// 'ACCEPTOR', 'CERTIFIED_RECIPIENT', 'FORM_FILLER',
        /// 'DELEGATE_TO_SIGNER', 'DELEGATE_TO_APPROVER',
        /// 'DELEGATE_TO_ACCEPTOR', 'DELEGATE_TO_CERTIFIED_RECIPIENT',
        /// 'DELEGATE_TO_FORM_FILLER'</param>
        public CounterSignerSetInfo(IList<CounterSignerInfo> counterSignerSetMemberInfos, string counterSignerSetRole)
        {
            CounterSignerSetMemberInfos = counterSignerSetMemberInfos;
            CounterSignerSetRole = counterSignerSetRole;
            CustomInit();
        }

        /// <summary>
        /// An initialization method that performs custom operations like setting defaults
        /// </summary>
        partial void CustomInit();

        /// <summary>
        /// Gets or sets information about the members of the counter signer
        /// set, currently we support only one member
        /// </summary>
        [JsonProperty(PropertyName = "counterSignerSetMemberInfos")]
        public IList<CounterSignerInfo> CounterSignerSetMemberInfos { get; set; }

        /// <summary>
        /// Gets or sets specify the role of counter signer set. Possible
        /// values include: 'SIGNER', 'APPROVER', 'ACCEPTOR',
        /// 'CERTIFIED_RECIPIENT', 'FORM_FILLER', 'DELEGATE_TO_SIGNER',
        /// 'DELEGATE_TO_APPROVER', 'DELEGATE_TO_ACCEPTOR',
        /// 'DELEGATE_TO_CERTIFIED_RECIPIENT', 'DELEGATE_TO_FORM_FILLER'
        /// </summary>
        [JsonProperty(PropertyName = "counterSignerSetRole")]
        public string CounterSignerSetRole { get; set; }

        /// <summary>
        /// Validate the object.
        /// </summary>
        /// <exception cref="ValidationException">
        /// Thrown if validation fails
        /// </exception>
        public virtual void Validate()
        {
            if (CounterSignerSetMemberInfos == null)
            {
                throw new ValidationException(ValidationRules.CannotBeNull, "CounterSignerSetMemberInfos");
            }
            if (CounterSignerSetRole == null)
            {
                throw new ValidationException(ValidationRules.CannotBeNull, "CounterSignerSetRole");
            }
            if (CounterSignerSetMemberInfos != null)
            {
                foreach (var element in CounterSignerSetMemberInfos)
                {
                    if (element != null)
                    {
                        element.Validate();
                    }
                }
            }
        }
    }
}
