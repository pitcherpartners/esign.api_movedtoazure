// Code generated by Microsoft (R) AutoRest Code Generator 1.2.2.0
// Changes may cause incorrect behavior and will be lost if the code is
// regenerated.

namespace PitcherPartners.eSign.WebClients.AdobeSign.Models
{
    using Microsoft.Rest;
    using Newtonsoft.Json;
    using PitcherPartners.eSign;
    using PitcherPartners.eSign.WebClients;
    using PitcherPartners.eSign.WebClients.AdobeSign;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;

    public partial class MegaSigns
    {
        /// <summary>
        /// Initializes a new instance of the MegaSigns class.
        /// </summary>
        public MegaSigns()
        {
          CustomInit();
        }

        /// <summary>
        /// Initializes a new instance of the MegaSigns class.
        /// </summary>
        /// <param name="megaSignList">An array of MegaSign parent
        /// agreements</param>
        public MegaSigns(IList<MegaSign> megaSignList)
        {
            MegaSignList = megaSignList;
            CustomInit();
        }

        /// <summary>
        /// An initialization method that performs custom operations like setting defaults
        /// </summary>
        partial void CustomInit();

        /// <summary>
        /// Gets or sets an array of MegaSign parent agreements
        /// </summary>
        [JsonProperty(PropertyName = "megaSignList")]
        public IList<MegaSign> MegaSignList { get; set; }

        /// <summary>
        /// Validate the object.
        /// </summary>
        /// <exception cref="ValidationException">
        /// Thrown if validation fails
        /// </exception>
        public virtual void Validate()
        {
            if (MegaSignList == null)
            {
                throw new ValidationException(ValidationRules.CannotBeNull, "MegaSignList");
            }
            if (MegaSignList != null)
            {
                foreach (var element in MegaSignList)
                {
                    if (element != null)
                    {
                        element.Validate();
                    }
                }
            }
        }
    }
}
