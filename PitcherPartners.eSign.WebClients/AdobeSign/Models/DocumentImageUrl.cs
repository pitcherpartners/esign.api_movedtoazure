// Code generated by Microsoft (R) AutoRest Code Generator 1.2.2.0
// Changes may cause incorrect behavior and will be lost if the code is
// regenerated.

namespace PitcherPartners.eSign.WebClients.AdobeSign.Models
{
    using Microsoft.Rest;
    using Newtonsoft.Json;
    using PitcherPartners.eSign;
    using PitcherPartners.eSign.WebClients;
    using PitcherPartners.eSign.WebClients.AdobeSign;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;

    public partial class DocumentImageUrl
    {
        /// <summary>
        /// Initializes a new instance of the DocumentImageUrl class.
        /// </summary>
        public DocumentImageUrl()
        {
          CustomInit();
        }

        /// <summary>
        /// Initializes a new instance of the DocumentImageUrl class.
        /// </summary>
        /// <param name="imageUrls">A list of objects representing all image
        /// URLs.(one per imagesize).</param>
        public DocumentImageUrl(IList<ImageUrl> imageUrls)
        {
            ImageUrls = imageUrls;
            CustomInit();
        }

        /// <summary>
        /// An initialization method that performs custom operations like setting defaults
        /// </summary>
        partial void CustomInit();

        /// <summary>
        /// Gets or sets a list of objects representing all image URLs.(one per
        /// imagesize).
        /// </summary>
        [JsonProperty(PropertyName = "imageUrls")]
        public IList<ImageUrl> ImageUrls { get; set; }

        /// <summary>
        /// Validate the object.
        /// </summary>
        /// <exception cref="ValidationException">
        /// Thrown if validation fails
        /// </exception>
        public virtual void Validate()
        {
            if (ImageUrls == null)
            {
                throw new ValidationException(ValidationRules.CannotBeNull, "ImageUrls");
            }
            if (ImageUrls != null)
            {
                foreach (var element in ImageUrls)
                {
                    if (element != null)
                    {
                        element.Validate();
                    }
                }
            }
        }
    }
}
