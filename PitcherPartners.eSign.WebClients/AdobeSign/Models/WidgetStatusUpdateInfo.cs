// Code generated by Microsoft (R) AutoRest Code Generator 1.2.2.0
// Changes may cause incorrect behavior and will be lost if the code is
// regenerated.

namespace PitcherPartners.eSign.WebClients.AdobeSign.Models
{
    using Microsoft.Rest;
    using Newtonsoft.Json;
    using PitcherPartners.eSign;
    using PitcherPartners.eSign.WebClients;
    using PitcherPartners.eSign.WebClients.AdobeSign;
    using System.Linq;

    public partial class WidgetStatusUpdateInfo
    {
        /// <summary>
        /// Initializes a new instance of the WidgetStatusUpdateInfo class.
        /// </summary>
        public WidgetStatusUpdateInfo()
        {
          CustomInit();
        }

        /// <summary>
        /// Initializes a new instance of the WidgetStatusUpdateInfo class.
        /// </summary>
        /// <param name="message">Display this custom message to the user when
        /// the widget is accessed. Note that this can contain wiki markup to
        /// include clickable links in the message. This is required if
        /// redirectUrl is not provided. Both message and redirectUrl can not
        /// be specified.</param>
        /// <param name="redirectUrl">Redirect the user to this URL when the
        /// widget is accessed. This is required if message is not provided.
        /// Both message and redirectUrl can not be specified.</param>
        /// <param name="value">The status to which the widget is to be
        /// updated. The possible values for this variable are ENABLE and
        /// DISABLE. Possible values include: 'DISABLE', 'ENABLE'</param>
        public WidgetStatusUpdateInfo(string message, string redirectUrl, string value)
        {
            Message = message;
            RedirectUrl = redirectUrl;
            Value = value;
            CustomInit();
        }

        /// <summary>
        /// An initialization method that performs custom operations like setting defaults
        /// </summary>
        partial void CustomInit();

        /// <summary>
        /// Gets or sets display this custom message to the user when the
        /// widget is accessed. Note that this can contain wiki markup to
        /// include clickable links in the message. This is required if
        /// redirectUrl is not provided. Both message and redirectUrl can not
        /// be specified.
        /// </summary>
        [JsonProperty(PropertyName = "message")]
        public string Message { get; set; }

        /// <summary>
        /// Gets or sets redirect the user to this URL when the widget is
        /// accessed. This is required if message is not provided. Both message
        /// and redirectUrl can not be specified.
        /// </summary>
        [JsonProperty(PropertyName = "redirectUrl")]
        public string RedirectUrl { get; set; }

        /// <summary>
        /// Gets or sets the status to which the widget is to be updated. The
        /// possible values for this variable are ENABLE and DISABLE. Possible
        /// values include: 'DISABLE', 'ENABLE'
        /// </summary>
        [JsonProperty(PropertyName = "value")]
        public string Value { get; set; }

        /// <summary>
        /// Validate the object.
        /// </summary>
        /// <exception cref="ValidationException">
        /// Thrown if validation fails
        /// </exception>
        public virtual void Validate()
        {
            if (Message == null)
            {
                throw new ValidationException(ValidationRules.CannotBeNull, "Message");
            }
            if (RedirectUrl == null)
            {
                throw new ValidationException(ValidationRules.CannotBeNull, "RedirectUrl");
            }
            if (Value == null)
            {
                throw new ValidationException(ValidationRules.CannotBeNull, "Value");
            }
        }
    }
}
