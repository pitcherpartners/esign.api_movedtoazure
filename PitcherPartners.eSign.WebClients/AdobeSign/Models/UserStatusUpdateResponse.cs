// Code generated by Microsoft (R) AutoRest Code Generator 1.2.2.0
// Changes may cause incorrect behavior and will be lost if the code is
// regenerated.

namespace PitcherPartners.eSign.WebClients.AdobeSign.Models
{
    using Microsoft.Rest;
    using Newtonsoft.Json;
    using PitcherPartners.eSign;
    using PitcherPartners.eSign.WebClients;
    using PitcherPartners.eSign.WebClients.AdobeSign;
    using System.Linq;

    public partial class UserStatusUpdateResponse
    {
        /// <summary>
        /// Initializes a new instance of the UserStatusUpdateResponse class.
        /// </summary>
        public UserStatusUpdateResponse()
        {
          CustomInit();
        }

        /// <summary>
        /// Initializes a new instance of the UserStatusUpdateResponse class.
        /// </summary>
        /// <param name="code">The result of the attempt to activate or
        /// deactivate the user. Possible values include: 'ALREADY_ACTIVE',
        /// 'ALREADY_INACTIVE', 'OK', 'RESET_PASSWORD_WORKFLOW_INITIATED',
        /// 'SET_PASSWORD_WORKFLOW_INITIATED'</param>
        /// <param name="userStatus">A status value showing the result of this
        /// operation. Possible values include: 'ACTIVE', 'INACTIVE',
        /// 'CREATED', 'PENDING', 'UNVERIFIED'</param>
        /// <param name="message">String result message if there was no
        /// error</param>
        public UserStatusUpdateResponse(string code, string userStatus, string message = default(string))
        {
            Code = code;
            Message = message;
            UserStatus = userStatus;
            CustomInit();
        }

        /// <summary>
        /// An initialization method that performs custom operations like setting defaults
        /// </summary>
        partial void CustomInit();

        /// <summary>
        /// Gets or sets the result of the attempt to activate or deactivate
        /// the user. Possible values include: 'ALREADY_ACTIVE',
        /// 'ALREADY_INACTIVE', 'OK', 'RESET_PASSWORD_WORKFLOW_INITIATED',
        /// 'SET_PASSWORD_WORKFLOW_INITIATED'
        /// </summary>
        [JsonProperty(PropertyName = "code")]
        public string Code { get; set; }

        /// <summary>
        /// Gets or sets string result message if there was no error
        /// </summary>
        [JsonProperty(PropertyName = "message")]
        public string Message { get; set; }

        /// <summary>
        /// Gets or sets a status value showing the result of this operation.
        /// Possible values include: 'ACTIVE', 'INACTIVE', 'CREATED',
        /// 'PENDING', 'UNVERIFIED'
        /// </summary>
        [JsonProperty(PropertyName = "userStatus")]
        public string UserStatus { get; set; }

        /// <summary>
        /// Validate the object.
        /// </summary>
        /// <exception cref="ValidationException">
        /// Thrown if validation fails
        /// </exception>
        public virtual void Validate()
        {
            if (Code == null)
            {
                throw new ValidationException(ValidationRules.CannotBeNull, "Code");
            }
            if (UserStatus == null)
            {
                throw new ValidationException(ValidationRules.CannotBeNull, "UserStatus");
            }
        }
    }
}
