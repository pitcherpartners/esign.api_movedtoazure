// Code generated by Microsoft (R) AutoRest Code Generator 1.2.2.0
// Changes may cause incorrect behavior and will be lost if the code is
// regenerated.

namespace PitcherPartners.eSign.WebClients.AdobeSign.Models
{
    using Microsoft.Rest;
    using Microsoft.Rest.Serialization;
    using Newtonsoft.Json;
    using PitcherPartners.eSign;
    using PitcherPartners.eSign.WebClients;
    using PitcherPartners.eSign.WebClients.AdobeSign;
    using System.Linq;

    public partial class MegaSignChildAgreement
    {
        /// <summary>
        /// Initializes a new instance of the MegaSignChildAgreement class.
        /// </summary>
        public MegaSignChildAgreement()
        {
          CustomInit();
        }

        /// <summary>
        /// Initializes a new instance of the MegaSignChildAgreement class.
        /// </summary>
        /// <param name="agreementId">The unique identifier of the
        /// agreement</param>
        /// <param name="displayDate">The display date for the
        /// agreement</param>
        /// <param name="esign">True if this is an e-sign document</param>
        /// <param name="name">Name of the Agreement</param>
        /// <param name="status">The current status of the agreement from the
        /// perspective of the user. Possible values include:
        /// 'WAITING_FOR_MY_SIGNATURE', 'WAITING_FOR_MY_APPROVAL',
        /// 'WAITING_FOR_MY_DELEGATION', 'OUT_FOR_SIGNATURE',
        /// 'OUT_FOR_APPROVAL', 'SIGNED', 'APPROVED', 'RECALLED',
        /// 'WAITING_FOR_FAXIN', 'ARCHIVED', 'FORM', 'EXPIRED', 'WIDGET',
        /// 'WAITING_FOR_AUTHORING'</param>
        public MegaSignChildAgreement(string agreementId, System.DateTime displayDate, bool esign, string name, string status)
        {
            AgreementId = agreementId;
            DisplayDate = displayDate;
            Esign = esign;
            Name = name;
            Status = status;
            CustomInit();
        }

        /// <summary>
        /// An initialization method that performs custom operations like setting defaults
        /// </summary>
        partial void CustomInit();

        /// <summary>
        /// Gets or sets the unique identifier of the agreement
        /// </summary>
        [JsonProperty(PropertyName = "agreementId")]
        public string AgreementId { get; set; }

        /// <summary>
        /// Gets or sets the display date for the agreement
        /// </summary>
        
        [JsonProperty(PropertyName = "displayDate")]
        public System.DateTime DisplayDate { get; set; }

        /// <summary>
        /// Gets or sets true if this is an e-sign document
        /// </summary>
        [JsonProperty(PropertyName = "esign")]
        public bool Esign { get; set; }

        /// <summary>
        /// Gets or sets name of the Agreement
        /// </summary>
        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the current status of the agreement from the
        /// perspective of the user. Possible values include:
        /// 'WAITING_FOR_MY_SIGNATURE', 'WAITING_FOR_MY_APPROVAL',
        /// 'WAITING_FOR_MY_DELEGATION', 'OUT_FOR_SIGNATURE',
        /// 'OUT_FOR_APPROVAL', 'SIGNED', 'APPROVED', 'RECALLED',
        /// 'WAITING_FOR_FAXIN', 'ARCHIVED', 'FORM', 'EXPIRED', 'WIDGET',
        /// 'WAITING_FOR_AUTHORING'
        /// </summary>
        [JsonProperty(PropertyName = "status")]
        public string Status { get; set; }

        /// <summary>
        /// Validate the object.
        /// </summary>
        /// <exception cref="ValidationException">
        /// Thrown if validation fails
        /// </exception>
        public virtual void Validate()
        {
            if (AgreementId == null)
            {
                throw new ValidationException(ValidationRules.CannotBeNull, "AgreementId");
            }
            if (Name == null)
            {
                throw new ValidationException(ValidationRules.CannotBeNull, "Name");
            }
            if (Status == null)
            {
                throw new ValidationException(ValidationRules.CannotBeNull, "Status");
            }
        }
    }
}
