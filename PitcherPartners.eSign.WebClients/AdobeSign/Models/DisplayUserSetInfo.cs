// Code generated by Microsoft (R) AutoRest Code Generator 1.2.2.0
// Changes may cause incorrect behavior and will be lost if the code is
// regenerated.

namespace PitcherPartners.eSign.WebClients.AdobeSign.Models
{
    using Microsoft.Rest;
    using Newtonsoft.Json;
    using PitcherPartners.eSign;
    using PitcherPartners.eSign.WebClients;
    using PitcherPartners.eSign.WebClients.AdobeSign;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;

    public partial class DisplayUserSetInfo
    {
        /// <summary>
        /// Initializes a new instance of the DisplayUserSetInfo class.
        /// </summary>
        public DisplayUserSetInfo()
        {
          CustomInit();
        }

        /// <summary>
        /// Initializes a new instance of the DisplayUserSetInfo class.
        /// </summary>
        /// <param name="displayUserSetMemberInfos">Displays the info about
        /// user set</param>
        /// <param name="displayUserSetName">The name of the display user set.
        /// Returned only, if the API caller is the sender of agreement.
        /// </param>
        public DisplayUserSetInfo(IList<DisplayUserInfo> displayUserSetMemberInfos, string displayUserSetName = default(string))
        {
            DisplayUserSetMemberInfos = displayUserSetMemberInfos;
            DisplayUserSetName = displayUserSetName;
            CustomInit();
        }

        /// <summary>
        /// An initialization method that performs custom operations like setting defaults
        /// </summary>
        partial void CustomInit();

        /// <summary>
        /// Gets or sets displays the info about user set
        /// </summary>
        [JsonProperty(PropertyName = "displayUserSetMemberInfos")]
        public IList<DisplayUserInfo> DisplayUserSetMemberInfos { get; set; }

        /// <summary>
        /// Gets or sets the name of the display user set. Returned only, if
        /// the API caller is the sender of agreement.
        /// </summary>
        [JsonProperty(PropertyName = "displayUserSetName")]
        public string DisplayUserSetName { get; set; }

        /// <summary>
        /// Validate the object.
        /// </summary>
        /// <exception cref="ValidationException">
        /// Thrown if validation fails
        /// </exception>
        public virtual void Validate()
        {
            if (DisplayUserSetMemberInfos == null)
            {
                throw new ValidationException(ValidationRules.CannotBeNull, "DisplayUserSetMemberInfos");
            }
            if (DisplayUserSetMemberInfos != null)
            {
                foreach (var element in DisplayUserSetMemberInfos)
                {
                    if (element != null)
                    {
                        element.Validate();
                    }
                }
            }
        }
    }
}
